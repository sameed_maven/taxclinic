<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blogs', function (Blueprint $table) {
            $table->date('publish_start_date')->nullable()->after('status');
            $table->date('publish_end_date')->nullable()->after('publish_start_date');
            $table->enum('scheduled_status',['0','1'])->default('0')->after('publish_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blogs', function (Blueprint $table) {
            $table->dropColumn('publish_start_date');
            $table->dropColumn('publish_end_date');
            $table->dropColumn('scheduled_status');
        });
    }
}
