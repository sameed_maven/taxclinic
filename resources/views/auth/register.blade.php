@include('frontend.includes.header')
<section class="pt-7 pb-7">
    @if(session('message'))
        <p class="alert alert-success">{{session('message')}}</p>
    @endif
    <div class="container">
        <div class="row align-items-lg-end">
            <div class="col-lg-4 col-md-5 col-md-offset-3">
                <div class="contact-form-wrap">
                    <div class="text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Register</h2>
                    </div>
                    <form class="contact-form" action="{{url('/customer_register')}}" method="post" id="registerform">
                        {{csrf_field()}}
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="text" id="fname" name="fname" placeholder="First Name"  {{(session('error')?'':'autofocus')}} value="{{old('fname')}}">
                            @if ($errors->has('fname'))
                                <span class="help-block">
                                <strong>{{ $errors->first('fname') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="text" id="lname" name="lname" placeholder="Last Name" value="{{old('lname')}}">
                            @if ($errors->has('lname'))
                                <span class="help-block">
                                <strong>{{ $errors->first('lname') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="email" id="email" name="email" placeholder="Email" value="{{old('email')}}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="password" id="password" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control mobile" onKeyPress="if(this.value.length==12) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" id="mobile" name="mobile" placeholder="Mobile" value="{{old('mobile')}}">
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control mobile" onKeyPress="if(this.value.length==12) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" id="phone" name="phone" placeholder="Phone" value="{{old('phone')}}">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="text" id="address" name="address" placeholder="Address" value="{{old('address')}}">
                            @if ($errors->has('address'))
                                <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="text" id="country" name="country" placeholder="Country" value="{{old('country')}}">
                            @if ($errors->has('country'))
                                <span class="help-block">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control cnic" onKeyPress="if(this.value.length==14) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" id="cnic" name="cnic" placeholder="CNIC" value="{{old('cnic')}}" {{(session('error')?'autofocus':'')}}>
                            @if ($errors->has('cnic') || session('error'))
                                <span class="help-block">
                                <strong>{{ (session('error'))?session('error'):$errors->first('cnic') }}</strong>
                            </span>
                            @endif
                        </div>
                        <button class="btn btn-primary btn-square btn-block" id="registerform_submit" name="submit" data-animate="fadeInUp" data-delay=".8">Register</button>
                        <br>
                        <hr>
                        <h3 style="text-align: center;">Or Register With</h3>
                        <div class="position-relative" align="center">
                            <a href="{{ url('/redirect_fb') }}" class="pg_load"><img src="{{URL::asset('frontend')}}/img/fb.png" alt="Facebook" style="width: 35px; height: 35px;"></a>
                            <a href="{{ url('/redirect_gl') }}" class="pg_load"><img src="{{URL::asset('frontend')}}/img/gmail.png" alt="Gmail" style="width: 35px; height: 35px;"></a>
                            <a href="{{ url('/redirect_tw') }}" class="pg_load"><img src="{{URL::asset('frontend')}}/img/twit.png" alt="Twitter" style="width: 35px; height: 35px;"></a>
                            <a href="{{ url('/redirect_ln') }}" class="pg_load"><img src="{{URL::asset('frontend')}}/img/link.png" alt="Linkined" style="width: 35px; height: 35px;"></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@include('frontend.includes.footer')
