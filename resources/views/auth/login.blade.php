@include('frontend.includes.header')
<section class="pt-7 pb-7">
    <div class="container">
        <div class="row align-items-lg-end">
            <div class="col-lg-4 col-md-5 col-md-offset-3">
                <div class="contact-form-wrap">
                    <div class="text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Login</h2>
                    </div>
                    @if(session('email_confirm'))
                        <p class="alert alert-danger">{{session('email_confirm')}}</p>
                    @endif
                    <form class="contact-form" action="{{route('login')}}" method="post" id="loginform">
                        {{csrf_field()}}
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="email" id="email" name="email" placeholder="Email" value="{{old('email')}}" autofocus>
                            @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input class="form-control" type="password" id="password" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <button class="btn btn-primary btn-square btn-block" id="loginform_submit" data-animate="fadeInUp" data-delay=".8">Login</button>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                            <input id="checkbox-signup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} style="float: left;">
                            <label for="checkbox-signup" style="margin-top: 8px; margin-left: 5px;"> Remember me </label>
                        </div>
                        <div class="position-relative" style="text-align: center;">
                            <a href="{{ route('password.request') }}">Forgot your password?</a>
                        </div>
                        <br>
                        <hr>
                        <h3 style="text-align: center;">Or Login With</h3>
                        <div class="position-relative" align="center">
                            <a href="{{url('/redirect_fb')}}" class="pg_load"><img src="{{URL::asset('frontend')}}/img/fb.png" style="width: 35px; height: 35px;"></a>
                            <a href="{{ url('/redirect_gl') }}" class="pg_load"><img src="{{URL::asset('frontend')}}/img/gmail.png" style="width: 35px; height: 35px;"></a>
                            <a href="{{url('/redirect_tw')}}"><img src="{{URL::asset('frontend')}}/img/twit.png" style="width: 35px; height: 35px;"></a>
                            <a href="{{ url('/redirect_ln') }}" class="pg_load"><img src="{{URL::asset('frontend')}}/img/link.png" style="width: 35px; height: 35px;"></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('frontend.includes.footer')
