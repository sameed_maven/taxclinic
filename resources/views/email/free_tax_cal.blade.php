<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>
</head>
<body>

<p><strong>Dear Sir,</strong></p>

<p>Welcome to Tax Clinic</p>

<p>We wanted to create a trustworthy place for you to find everything you need to know about Taxation.</p>

<p>Your Calculation about Taxation is mentioned below</p>

{!! $content !!}

<p>Thank You</p>
<p>Tax Clinic Team</p>

<p>Email: <a href="mailto:contact@TaxClinic.pk">contact@TaxClinic.pk</a></p>
<p>Web Site: <a href="www.taxclinic.pk">www.taxclinic.pk</a></p>

<p>As per the <a href="http://taxcalculator.pk/tax-ordinance-2001-2017.pdf">Income tax ordinance</a> passed by Government of Pakistan, following slabs and income tax rates will be applicable for salaried persons for the year 2017-2018.</p>

</body>
</html>