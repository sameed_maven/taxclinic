@include('frontend.includes.header')
@if(session('message'))
   <p class="alert alert-success">{{session('message')}}</p>
@endif
      <section class="position-relative bg-light pt-4 pb-4">
         <div id="particles_js"></div>
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6">
                  <div class="banner-content">
                     <h1 data-animate="fadeInUp" data-delay="1.2">World’s Best Tax Service Provider In 2018</h1>
                     <h2 data-animate="fadeInUp" data-delay="1.3"><span class="typed"></span></h2>
                     <ul class="list-inline" data-animate="fadeInUp" data-delay="1.4">
                        {{--<li><a href="#" class="btn btn-primary">Learn More</a></li>--}}
                        {{--<li><a href="#" class="btn">Check Offer’s <i class="fas fa-caret-right"></i></a></li>--}}
                     </ul>
                  </div>
               </div>
               <div class="col-lg-6 d-none d-lg-block">
                  <div class="banner-image"> <img src="{{URL::asset('frontend')}}/img/slide.png" alt="" data-animate="fadeInUp" data-delay="1.5" data-depth="0.2"></div>
               </div>
            </div>
         </div>
      </section>
      <section class="pt-4 pb-5-5 " style="display:block;">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="single-feature " data-animate="fadeInUp" data-delay=".1">

                     <h1 data-animate="fadeInUp" data-delay="1.3" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 1.3s;">Who we are</h1>
                     <p>Tax Clinic is providing professional services with ease of communication through online portal with our consultants available. We respond to your queries and submit your tax return professionally. You don’t need to visit any office, upload / share information through our secure portal with respect to your income, tax and wealth etc, our consultants prepare the computation and shared with you before they submit the same to FBR. Our consultants remain vigilant for any notices etc once you remain enrolled with us. Moreover, relevant updates will be shared with you directly/indirectly affecting you.
                     </p>
                  </div>
               </div>


            </div>
         </div>
      </section>
      <section>
         <div class="services-title position-relative pt-7" data-bg-img="img/buildings.jpg">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-xl-6 col-lg-8">
                     <div class="section-title text-center">
                        <h2 class="text-white" data-animate="fadeInUp" data-delay=".1">Why You Need Our Service</h2>
                        <p  class="text-white" data-animate="fadeInUp" data-delay=".3">World’s Best Tax Service Provider In 2018</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="services-wrap bg-white position-relative pt-5 pb-5">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-6">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".05">
                        <img src="{{URL::asset('frontend')}}/img/services-icons/tax.png" alt="" data-no-retina class="svg">
                        <h4>Income Tax</h4>
                        <span style="text-align:justify;">There is an exhaustive list of person who are required under the Income Tax Ordinance, 2001 “ITO” to file return of income through Online portal of FBR. The online process for registration of individual is simple, however, in order to incorporate the correct information about the particulars with respect to status such as Business/ Salaried/ Other, it is advised to get assistance from Professionals.</span>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".1">
                        <img src="{{URL::asset('frontend')}}/img/services-icons/bill.png" alt="" data-no-retina class="svg">
                        <h4>Sales Tax</h4>
                        <span>Our Sales Tax Act, 1990 “STA” is a hybrid of General Sales Tax and Value Added Tax, over the years various amendments made in the law and interpretation of various of provisions is governed through judgement of appellate authorities.

</span>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".15">
                        <img src="{{URL::asset('frontend')}}/img/services-icons/business-management.png" alt="" data-no-retina class="svg">
                        <h4>Provinical Sales Tax</h4>
                        <span>Consequent upon the 18th Constitutional Amendment (specifically in relation to item No. 49 of Part A of the Fourth Schedule thereof) and pursuant to Articles 8 and 9(2) of the 7th NFC Award, notified in 2010, four Provincial Governments enacted their Services Act excluding Services in Islamabad covered under Islamabad Capital Territory Ordinance, currently.</span>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".2">
                        <img src="{{URL::asset('frontend')}}/img/services-icons/business.png" alt="" data-no-retina class="svg">
                        <h4>Corporate</h4>
                        <span>Company limited by shares, guarantee or charitable / not for profit organizations registered with Securities and Exchange Commission of Pakistan “SECP”.
<br>
There are several legal requirements in order to incorporate a company with SECP or in case of winding up of a company.

</span>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".25">
                        <img src="{{URL::asset('frontend')}}/img/services-icons/mission.png" alt="" data-no-retina class="svg">
                        <h4>Business Setup</h4>
                        <span>Tax clinic will help with registration of Company whether corporate, individual or partnership firm based on your need basis. We also advise the best form of business form suitable based on understanding of your business.
<br>
Taxation of various forms of business having different tax structure, therefore, it is imperative to have proper advice beforehand.</span>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" style="display:none;">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".3">
                        <img src="{{URL::asset('frontend')}}/img/icons/virus.svg" alt="" data-no-retina class="svg">
                        <h4>Online Filling</h4>
                        <span>At vero eos et accusamus et iusto odioissimos bland very voluptatum.</span>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" style="display:none;">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".35">
                        <img src="{{URL::asset('frontend')}}/img/icons/bandwidth.svg" alt="" data-no-retina class="svg">
                        <h4>Advisory</h4>
                        <span>At vero eos et accusamus et iusto odioissimos bland very voluptatum.</span>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" style="display:none;">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".4">
                        <img src="{{URL::asset('frontend')}}/img/icons/trial.svg" alt="" data-no-retina class="svg">
                        <h4>Representation</h4>
                        <span>At vero eos et accusamus et iusto odioissimos bland very voluptatum.</span>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" style="display:none;">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".45">
                        <img src="{{URL::asset('frontend')}}/img/icons/website.svg" alt="" data-no-retina class="svg">
                        <h4>Incorporation</h4>
                        <span>At vero eos et accusamus et iusto odioissimos bland very voluptatum.</span>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6"  style="display:none;">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".5">
                        <img src="{{URL::asset('frontend')}}/img/icons/settings.svg" alt="" data-no-retina class="svg">
                        <h4>Winding Up</h4>
                        <span>At vero eos et accusamus et iusto odioissimos bland very voluptatum.</span>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6" style="display:none;">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".55">
                        <img src="{{URL::asset('frontend')}}/img/icons/team.svg" alt="" data-no-retina class="svg">
                        <h4>Regulatory Framework</h4>
                        <span>At vero eos et accusamus et iusto odioissimos bland very voluptatum.</span>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-6"  style="display:none;">
                     <div class="single-service" data-animate="fadeInUp" data-delay=".6">
                        <img src="{{URL::asset('frontend')}}/img/icons/money.svg" alt="" data-no-retina class="svg">
                        <h4>Tax Laws</h4>
                        <span>At vero eos et accusamus et iusto odioissimos bland very voluptatum.</span>
                     </div>
                  </div>
               </div>
               <div class="roboto text-center font-weight-medium" data-animate="fadeInUp" data-delay=".65">
                  <p>If you have any questions in your mind, Just <a href="contact">click here</a> to write </p>
               </div>
            </div>
         </div>
      </section>
      <section class="pt-7 pb-7 bg-light" style="display:none;">
         <div class="container">
            <div class="section-title text-center">
               <h2 data-animate="fadeInUp" data-delay=".1">What client’s say about us</h2>
            </div>
            <div class="swiper-container review-slider">
               <div class="swiper-wrapper">
                  <div class="swiper-slide single-review-slide">
                     <h4>Marsha C. Meyer <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i></h4>
                     <span>Melbourne, Australia</span>
                     <p>I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give complete account of the system, and expound the actual teachings of happiness. No one rejects, dislikes, or avoids.</p>
                  </div>
                  <div class="swiper-slide single-review-slide">
                     <h4>Bns H. Jabed <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i></h4>
                     <span>Noakhali, Bangladesh</span>
                     <p>I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give complete account of the system, and expound the actual teachings of happiness. No one rejects, dislikes, or avoids.</p>
                  </div>
                  <div class="swiper-slide single-review-slide">
                     <h4>Cathy S. Knight <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i></h4>
                     <span>California, United States</span>
                     <p>I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give complete account of the system, and expound the actual teachings of happiness. No one rejects, dislikes, or avoids.</p>
                  </div>
                  <div class="swiper-slide single-review-slide">
                     <h4>Cathy S. Knight <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i></h4>
                     <span>California, United States</span>
                     <p>I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give complete account of the system, and expound the actual teachings of happiness. No one rejects, dislikes, or avoids.</p>
                  </div>
                  <div class="swiper-slide single-review-slide">
                     <h4>Cathy S. Knight <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i></h4>
                     <span>California, United States</span>
                     <p>I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give complete account of the system, and expound the actual teachings of happiness. No one rejects, dislikes, or avoids.</p>
                  </div>
               </div>
            </div>
            <div class="swiper-pagination review-pagination position-static"></div>
         </div>
      </section>
      <section class="pt-7 pb-7" style="display:none;">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-md-4 d-none d-md-block">
                  <div class="text-center" data-animate="fadeInUp" data-delay=".1"> <img src="{{URL::asset('frontend')}}/img/mobile.png" alt=""></div>
               </div>
               <div class="col-lg-6 col-md-8">
                  <div class="app-info">
                     <h2 data-animate="fadeInUp" data-delay=".1">Download Your Application</h2>
                     <p data-animate="fadeInUp" data-delay=".3">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem</p>
                     <ul class="apps-list list-inline">
                        <li data-animate="fadeInUp" data-delay=".5"><a href="#"><img src="{{URL::asset('frontend')}}/img/play-store.png" alt=""></a></li>
                        <li data-animate="fadeInUp" data-delay=".6"><a href="#"><img src="{{URL::asset('frontend')}}/img/app-store.png" alt=""></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="servers pt-7 bg-light" style="display:none;">
         <div class="container">
            <div class="row">
               <div class="col-xl-4 col-lg-5">
                  <div class="section-title">
                     <h2 data-animate="fadeInUp" data-delay=".1">865 Server In 197 Country</h2>
                     <p data-animate="fadeInUp" data-delay=".2">There are many variations of passages of Lorem Ipsum ailable, but the majority have suffered hmour</p>
                  </div>
                  <ul class="data-centers list-unstyled list-item clearfix">
                     <li data-animate="fadeInUp" data-delay=".1"><i class="fas fa-caret-right"></i>North America (201)</li>
                     <li data-animate="fadeInUp" data-delay=".2"><i class="fas fa-caret-right"></i>South America (169)</li>
                     <li data-animate="fadeInUp" data-delay=".3"><i class="fas fa-caret-right"></i>Europe (151)</li>
                     <li data-animate="fadeInUp" data-delay=".4"><i class="fas fa-caret-right"></i>Australia/Oceania (142)</li>
                     <li data-animate="fadeInUp" data-delay=".5"><i class="fas fa-caret-right"></i>Asia (70)</li>
                     <li data-animate="fadeInUp" data-delay=".6"><i class="fas fa-caret-right"></i>Africa (40)</li>
                  </ul>
                  <a href="#" class="btn server-btn" data-animate="fadeInUp" data-delay=".7">View Payment Method <i class="fas fa-caret-right"></i></a>
               </div>
               <div class="col-xl-8 col-lg-7 d-none d-lg-block">
                  <div class="server-map"> <img src="{{URL::asset('frontend')}}/img/servers.png" alt=""></div>
               </div>
            </div>
         </div>
      </section>
      <section class="clients-wrap pt-4 pb-4" style="display:none;">
         <div class="container">
            <ul class="our-clients list-unstyled d-md-flex align-items-md-center justify-content-md-between m-0">
               <li data-animate="fadeInUp" data-delay=".1"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand1.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".2"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand2.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".3"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand3.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".4"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand4.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".5"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand5.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".6"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand6.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".7"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand7.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".8"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand8.png" alt=""></a></li>
               <li data-animate="fadeInUp" data-delay=".9"> <a href="#" target="_blank"><img src="{{URL::asset('frontend')}}/img/brands/brand9.png" alt=""></a></li>
            </ul>
         </div>
      </section>
@include('frontend.includes.footer')