@include('frontend.includes.header')
<section class="page-title-wrap position-relative bg-light">
    <div id="particles_js"></div>
    <div class="container">
        <div class="row">
            <div class="col-11">
                <div class="page-title position-relative pt-5 pb-5">
                    <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url("/")}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Services</a></li>
                    </ul>
                    <h1 data-animate="fadeInUp" data-delay="1.3">Business Setup</h1>
                </div>
            </div>
            <div class="col-1">
                <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="services-title position-relative pt-7" data-bg-img="{{URL::asset('frontend')}}/img/buildings.jpg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        {{--<h2 class="text-white" data-animate="fadeInUp" data-delay=".1">Why You Need Our Service</h2>--}}
                        {{--<p class="text-white" data-animate="fadeInUp" data-delay=".3">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-wrap bg-white position-relative pt-3 pb-3">
        <div class="container">
            <br>
          <p>Tax clinic will help with registration of Company whether corporate, individual or partnership firm based on your need basis. We also advise the best form of business form suitable based on understanding of your business.
          </p>
            <p>Taxation of various forms of business having different tax structure, therefore, it is imperative to have proper advice beforehand.
            </p>
            <p>Tax Clinic help you in all the above forms stated above.
            </p>
            <p>We will also get your business enrolled with Taxing authorities whether Federal or Provincial authorities so that you remain at ease and focus on your business.
            </p>
            <p>Our consultant at our portal will remain focused to serve you with all your business setup needs.
            </p>
        </div>
    </div>
</section>
@include('frontend.includes.footer')