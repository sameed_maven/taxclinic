@include('frontend.includes.header')
<section class="page-title-wrap position-relative bg-light">
    <div id="particles_js"></div>
    <div class="container">
        <div class="row">
            <div class="col-11">
                <div class="page-title position-relative pt-3 pb-3">
                    <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Tax Consultancy</a></li>
                    </ul>
                    <h1 data-animate="fadeInUp" data-delay="1.3">Free Tax Consultancy</h1>
                </div>
            </div>
            <div class="col-1">
                <div class="world-map position-relative"> <img src="img/map.svg" alt="" data-no-retina class="svg"></div>
            </div>
        </div>
    </div>
</section>
<section class="pt-7 pb-7">
    <div class="container">
        <div class="row justify-content-center">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            <div class="col-md-11">
                <div class="about-us-title text-center">
                    <h2 data-animate="fadeInUp" data-delay=".1">Have Any Questions In Your Mind?</h2>
                </div>
            </div>
        </div>
        <div class="border-bottom pt-4 pb-4">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="single-faq-wrap">
                        <h2 data-animate="fadeInUp" data-delay=".1" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.1s;">General Questions &amp; Answers</h2>
                        @foreach($inquiry as $inquiry)
                        <h4 data-animate="fadeInUp" data-delay=".2" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.2s;"><svg class="svg-inline--fa fa-question-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg><!-- <i class="fas fa-question-circle"></i> --> {{$inquiry->message}}?</h4>
                        <p data-animate="fadeInUp" data-delay=".3" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.3s;">{{(isset($inquiry->inquiry_reply[0]))?$inquiry->inquiry_reply[0]->message:''}}</p>
                        @endforeach
                        {{--<h4 data-animate="fadeInUp" data-delay=".4" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.4s;"><svg class="svg-inline--fa fa-question-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg><!-- <i class="fas fa-question-circle"></i> --> What's the difference between a Proxy and VPN Service?</h4>--}}
                        {{--<p data-animate="fadeInUp" data-delay=".5" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.5s;">At vero eos et accusamus et iusto odio dignissimos ducimus ui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati piditate non ilique sunt in culpa qui officia deserunt illitia test laborum et dolorum fuga.</p>--}}
                        {{--<h4 data-animate="fadeInUp" data-delay=".6" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.6s;"><svg class="svg-inline--fa fa-question-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg><!-- <i class="fas fa-question-circle"></i> --> Can I use your VPN instead of my current ISP connection?</h4>--}}
                        {{--<p data-animate="fadeInUp" data-delay=".7" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.7s;">At vero eos et accusamus et iusto odio dignissimos ducimus ui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati piditate non ilique sunt in culpa qui officia deserunt illitia test laborum et dolorum fuga.</p>--}}
                    </div>
                </div>
                <div class="col-md-6">
                        <div class="contact-form-wrap">
                            <div class="text-center">
                                <h2 data-animate="fadeInUp" data-delay=".1">Inquiry Form</h2>
                                <p data-animate="fadeInUp" data-delay=".2">Fill up the form. Your e-mail will not be published. Required fields are marked by <span class="text-danger font-weight-bold">*</span></p>
                            </div>
                            <form class="contact-form" action="{{url('/inquiry')}}" method="post" id="inquiry_form" name="inquiry_form">
                                {{csrf_field()}}
                                <div class="position-relative" data-animate="fadeInUp" data-delay=".3">
                                    <input type="text" name="fname" placeholder="First Name*" class="form-control" {{(isset($customer[0]->first_name) && $customer[0]->first_name != ' ')?'readonly':''}} value="{{(isset($customer[0]->first_name) && $customer[0]->first_name != ' ')?decrypt($customer[0]->first_name):''}}">
                                </div>
                                <div class="position-relative" data-animate="fadeInUp" data-delay=".3">
                                    <input type="text" name="lname" placeholder=" Last Name*" class="form-control" {{(isset($customer[0]->last_name) && $customer[0]->last_name != ' ')?'readonly':''}} value="{{(isset($customer[0]->last_name) && $customer[0]->last_name != ' ')?decrypt($customer[0]->last_name):''}}">
                                </div>
                                <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                                    <input type="email" name="email" placeholder="E-mail*"class="form-control" {{(isset($customer))?'readonly':''}} value="{{(isset($customer[0]->user))?$customer[0]->user->email:''}}">
                                </div>
                                <div class="position-relative" data-animate="fadeInUp" data-delay=".5">
                                    <input  name="telephone" onKeyPress="if(this.value.length==12) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" {{(isset($customer[0]->mobile_number) && $customer[0]->mobile_number != ' ')?'readonly':''}} placeholder="Mobile Number*" class="form-control mobile" value="{{(isset($customer[0]->mobile_number) && $customer[0]->mobile_number != ' ')?decrypt($customer[0]->mobile_number):''}}">
                                </div>
                                <div class="position-relative" data-animate="fadeInUp" data-delay=".7">
                                    <textarea name="message" placeholder="Write message*" class="form-control"></textarea>
                                </div>
                                <button class="btn btn-primary btn-square btn-block" type="submit" id="inquiry_form_submit" data-animate="fadeInUp" data-delay=".8">Send message</button>
                            </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
{{--<div id="inquiry_modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<h4 class="modal-title" id="myModalLabel">Success Message</h4>--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
            {{--</div>--}}
            {{--<div class="modal-body">--}}
                {{--<h4 class="inquiry_message"></h4>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /.modal-content -->--}}
    {{--</div>--}}
    {{--<!-- /.modal-dialog -->--}}
{{--</div>--}}
@include('frontend.includes.footer')