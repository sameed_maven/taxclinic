@include('frontend.includes.header')
      <section class="page-title-wrap position-relative bg-light">
         <div id="particles_js"></div>
         <div class="container">
            <div class="row">
               <div class="col-11">
                  <div class="page-title position-relative pt-5 pb-5">
                     <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="index.html">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Contact</a></li>
                     </ul>
                     <h1 data-animate="fadeInUp" data-delay="1.3">Contact</h1>
                  </div>
               </div>
               <div class="col-1">
                  <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
               </div>
            </div>
         </div>
      </section>
      <section class="pt-7 pb-7">
         <div class="container">
            <div class="row align-items-lg-end">
               <div class="col-lg-8 col-md-7">
                  <div class="section-title">
                     <h2 data-animate="fadeInUp" data-delay=".1">Select your queries & write to our experts.</h2>
                  </div>
                  <div class="queries-wrap">
                     <div class="row">
                        <div class="col-lg-6 col-md-12">
                           <div class="single-query d-flex align-items-center" data-animate="fadeInUp" data-delay=".05">
                              <div class="query-icon"> <img src="{{URL::asset('frontend')}}/img/services-icons/tax.png" alt="" data-no-retina class="svg"></div>
                              <div class="query-info">
                                 <h4>Income Tax</h4>
                                 <span>There is an exhaustive list of person who are required under the Income Tax.</span> <a href="{{url('income-tax')}}">READ MORE</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                           <div class="single-query d-flex align-items-center" data-animate="fadeInUp" data-delay=".15">
                              <div class="query-icon"> <img src="{{URL::asset('frontend')}}/img/services-icons/bill.png" alt="" data-no-retina class="svg"></div>
                              <div class="query-info">
                                 <h4>Sales Tax</h4>
                                 <span>AOur Sales Tax Act, 1990 “STA” is a hybrid of General Sales Tax.</span> <a href="{{url('sales-tax')}}">READ MORE</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                           <div class="single-query d-flex align-items-center" data-animate="fadeInUp" data-delay=".25">
                              <div class="query-icon"> <img src="{{URL::asset('frontend')}}/img/services-icons/business-management.png" alt="" data-no-retina class="svg"></div>
                              <div class="query-info">
                                 <h4>Provinical Sales Tax</h4>
                                 <span>Provincial Governments enacted their Services Act excluding Services in Islamabad.</span> <a href="{{url('provincial-tax')}}">READ MORE</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                           <div class="single-query d-flex align-items-center" data-animate="fadeInUp" data-delay=".35">
                              <div class="query-icon"> <img src="{{URL::asset('frontend')}}/img/services-icons/business.png" alt="" data-no-retina class="svg"></div>
                              <div class="query-info">
                                 <h4>Corporate</h4>
                                 <span>There are several legal requirements in order to incorporate a company with SECP.</span> <a href="{{url('corporate')}}">READ MORE</a>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                           <div class="single-query d-flex align-items-center" data-animate="fadeInUp" data-delay=".45">
                              <div class="query-icon"> <img src="{{URL::asset('frontend')}}/img/services-icons/mission.png" alt="" data-no-retina class="svg"></div>
                              <div class="query-info">
                                 <h4>Business Setup</h4>
                                 <span>We also advise the best form of business form suitable based on understanding of your business.</span> <a href="{{url('business-setup')}}">READ MORE</a>
                              </div>
                           </div>
                        </div>
                        {{--<div class="col-lg-6 col-md-12">--}}
                           {{--<div class="single-query d-flex align-items-center" data-animate="fadeInUp" data-delay=".55">--}}
                              {{--<div class="query-icon"> <img src="{{URL::asset('frontend')}}/img/icons/press-query.svg" alt="" data-no-retina class="svg"></div>--}}
                              {{--<div class="query-info">--}}
                                 {{--<h4>Press Area Queries</h4>--}}
                                 {{--<span>At vero eos et accusamus et iusto oissimos bland very voluptatum.</span> <a href="mailto:press@example.com">press@example.com</a>--}}
                              {{--</div>--}}
                           {{--</div>--}}
                        {{--</div>--}}
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-5">
                  <div class="contact-form-wrap">
                     <div class="text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Contact Form</h2>
                        <p data-animate="fadeInUp" data-delay=".2">Fill up the form. Your e-mail will not be published. Required fields are marked by <span class="text-danger font-weight-bold">*</span></p>
                     </div>
                     <form class="contact-form" action="{{url('/contact')}}" method="post" id="contact_form">
                         {{csrf_field()}}
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".3">
                           <input type="text" name="name" placeholder="Name*" class="form-control">
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".4">
                           <input type="email" name="email" placeholder="E-mail*"class="form-control">
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".5">
                           <input type="number" name="telephone" onKeyPress="if(this.value.length==11) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" placeholder="Telephone*" class="form-control">
                        </div>
                        <div class="position-relative" data-animate="fadeInUp" data-delay=".7">
                           <textarea name="message" placeholder="Write message*" class="form-control"></textarea>
                        </div>
                        <button class="btn btn-primary btn-square btn-block" id="contact_form_submit" data-animate="fadeInUp" data-delay=".8">Send message</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
<div id="contact_modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Success Message</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         </div>
         <div class="modal-body">
            <h4 class="contact_message"></h4>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
@include('frontend.includes.footer')