@include('frontend.includes.header')
<section class="page-title-wrap position-relative bg-light">
    <div id="particles_js"></div>
    <div class="container">
        <div class="row">
            <div class="col-11">
                <div class="page-title position-relative pt-5 pb-5">
                    <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url("/")}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Services</a></li>
                    </ul>
                    <h1 data-animate="fadeInUp" data-delay="1.3">Provincial Tax</h1>
                </div>
            </div>
            <div class="col-1">
                <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="services-title position-relative pt-7" data-bg-img="{{URL::asset('frontend')}}/img/buildings.jpg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        {{--<h2 class="text-white" data-animate="fadeInUp" data-delay=".1">Why You Need Our Service</h2>--}}
                        {{--<p class="text-white" data-animate="fadeInUp" data-delay=".3">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-wrap bg-white position-relative pt-3 pb-3">
        <div class="container">
            <br>
            <p>Consequent upon the 18th Constitutional Amendment (specifically in relation to item No. 49 of Part A of the Fourth Schedule thereof) and pursuant to Articles 8 and 9(2) of the 7th NFC Award, notified in 2010, four Provincial Governments enacted their Services Act excluding Services in Islamabad covered under Islamabad Capital Territory Ordinance, currently.
            </p>
            <p>Tax Clinic will provide services with respect to registration with these provincial authorities if the services provided in the respective provinces.
            </p>
            <p>The professional team get the basic information and the get the business details through online portal without any physical presence to explain the business.
            </p>
            <p>We also help you to file your monthly sales tax return and also represent you in case of any query raised by FBR or Audit proceedings initiated by them.
            </p>
            <p>In case of any ST litigation we will file appeal to appropriate forum to obtain relief from undue tax burden.
            </p>
        </div>
    </div>
</section>
@include('frontend.includes.footer')