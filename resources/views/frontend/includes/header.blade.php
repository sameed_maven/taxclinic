<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tax Clinic</title>
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,500i,700%7CRoboto:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/plugins/swiper/swiper.min.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/plugins/magnific-popup/magnific-popup.min.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/plugins/color-switcher/color-switcher.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/css/plugin.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/css/style.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/css/responsive.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/css/colors/theme-color-1.css">
    <link rel="stylesheet" href="{{URL::asset('frontend')}}/css/custom.css">

    <script src="{{URL::asset('frontend')}}/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div class="preLoader"></div>
<div class="se-pre-con" style="display: none;"></div>
<header class="header">
    <div class="main-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 col-md-3 col-sm-3 col-9">
                    <div class="logo" data-animate="fadeInUp" data-delay=".7"> <a href="{{url('/')}}"> <img style="max-width: 70%;" src="{{URL::asset('frontend')}}/img/taxlogo.png" alt=""> </a></div>
                </div>
                <div class="col-lg-7 col-md-5 col-sm-3 col-3">
                    <nav data-animate="fadeInUp" data-delay=".9">
                        <div class="header-menu">
                            <ul>
                                <li class="active"><a href="{{url('/')}}">Home</a></li>
                                <li>
                                    <a href="#">Service<i class="fas fa-caret-down"></i></a>
                                    <ul>
                                        <li>
                                            <a href="#">Income Tax<i class="fas fa-caret-right"></i></a>
                                            <ul>
                                                <li><a href="{{url('income-tax')}}">Online Filling</a></li>
                                                <li><a href="{{url('income-tax')}}">Advisory</a></li>
                                                <li><a href="{{url('income-tax')}}">Representation</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Sales Tax<i class="fas fa-caret-right"></i></a>
                                            <ul>
                                                <li><a href="{{url('sales-tax')}}">Filling</a></li>
                                                <li><a href="{{url('sales-tax')}}">Advisory</a></li>
                                                <li><a href="{{url('sales-tax')}}">Representation</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Provinical Sales Tax<i class="fas fa-caret-right"></i></a>
                                            <ul>
                                                <li><a href="{{url('provincial-tax')}}">Filling</a></li>
                                                <li><a href="{{url('provincial-tax')}}">Advisory</a></li>
                                                <li><a href="{{url('provincial-tax')}}">Representation</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Corporate<i class="fas fa-caret-right"></i></a>
                                            <ul>
                                                <li><a href="{{url('corporate')}}">Filling</a></li>
                                                <li><a href="{{url('corporate')}}">Incorporation</a></li>
                                                <li><a href="{{url('corporate')}}">Winding Up</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Business Setup<i class="fas fa-caret-right"></i></a>
                                            <ul>
                                                <li><a href="{{url('business-setup')}}">Company Registeration</a></li>
                                                <li><a href="{{url('business-setup')}}">Regulatory Framework</a></li>
                                                <li><a href="{{url('business-setup')}}">Tax Laws</a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li><a href="{{url('blog')}}">Blog</a></li>
                                <li><a href="{{url('inquiry')}}">Tax Consultancy</a></li>
                                <li>
                                    <a href="#">Calculator</a>
                                    <ul>
                                        <li>
                                            <a href="{{url('free-tax-cal')}}">Free Income Tax Calculator</a>
                                        </li>
                                        <li>
                                            <a href="#">Free Witholding Income Tax Calculator</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="{{url('contact')}}">Contact</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 d-none d-sm-block">
                    <ul class="client-area text-right list-inline m-0" data-animate="fadeInUp" data-delay="1.1">
                        <li class="dropdown">
                            <a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{(Auth::user())?ucwords(str_replace('_',' ',Auth::user()->name)):'My Account'}} <i class="fas fa-caret-down"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right client-links " aria-labelledby="dropdownMenuLink">
                                @if(Auth::guest())
                                    <li><a href="{{route('login')}}">Sign In</a></li>
                                    <li><a href="{{route('register')}}">Sign Up</a></li>
                                @else
                                    <li><a href="{{url('/'.Auth::user()->roles->first()->name.'/dashboard')}}">Dashboard</a></li>
                                    <li><a href="#">Profile</a></li>
                                    <li>

                                        <a href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>