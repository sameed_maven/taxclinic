<footer class="main-footer bg-primary pt-4">
    <div class="container">
        <div class="row pb-3">
            <div class="col-md-4">
                <div class="footer-info">
                    <h3 class="text-white" data-animate="fadeInUp" data-delay="0">About Us</h3>
                    <p data-animate="fadeInUp" data-delay=".05">Tax Clinic is providing professional services with ease of communication through online portal with our consultants available. We respond to your queries and submit your tax return professionally.</p>
                    <ul class="footer-contacts list-unstyled">
                        <li data-animate="fadeInUp" data-delay=".1"> <i class="fas fa-phone"></i>  <a href="tel:0300 824 0498">0300 824 0498</a></li>
                        <li data-animate="fadeInUp" data-delay=".15"> <i class="fas fa-envelope"></i> <a href="javascript:void(0);">info@taxclinic.pk</a></li>
                        <li data-animate="fadeInUp" data-delay=".2"> <i class="fas fa-map-marker-alt"></i> <span>701, 7th Floor, Caesar Tower, Sharha-e-Faisal, Opp Aisha Bawany, Karachi</span></li>
                    </ul>
                    <ul class="social-links list-inline mb-0">
                        <li data-animate="fadeInUp" data-delay=".25"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li data-animate="fadeInUp" data-delay=".3"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li data-animate="fadeInUp" data-delay=".35"><a href="https://plus.google.com/discover" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                        <li data-animate="fadeInUp" data-delay=".4"><a href="https://www.linkedin.com/uas/login" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-posts">
                    <h3 class="text-white" data-animate="fadeInUp" data-delay=".5">Latest News/Blogs</h3>
                    @if(!empty($footer_blog))
                        @foreach($footer_blog as $value)
                    <div class="single-footer-post clearfix" data-animate="fadeInUp" data-delay=".55">
                        <a href="#" class="float-left"> <img src="{{url('images/blog',$value->image_url)}}" alt="" style="width: 85px; height: 85px;"> </a> <span>Posted on <a href="#">{{ $value->created_at }}</a></span>
                        <h4 class="cabin font-weight-normal"><a href="{{ url('single-blog',$value->id) }}">{{ $value->title }}</a></h4>
                        <p>Expand its civil service aviations web hosting powerhouse go daddy.</p>
                    </div>
                        @endforeach
                    @endif
                    <a href="{{url('blog')}}" class="roboto text-uppercase" data-animate="fadeInUp" data-delay=".65">View All Blog Posts <i class="fas fa-caret-right"></i></a>
                </div>
            </div>
            <div class="col-md-4" style="display:none;">
                <div class="footer-newsletter">
                    <h3 class="text-white" data-animate="fadeInUp" data-delay=".65">Newsletter</h3>
                    <p data-animate="fadeInUp" data-delay=".7">In major hiring push, web hosting powerhouse go daddy to expand its civil service aviations</p>
                    <form action="https://themelooks.us13.list-manage.com/subscribe/post?u=79f0b132ec25ee223bb41835f&amp;id=f4e0e93d1d" method="post" name="mc-embedded-subscribe-form" target="_blank">
                        <div class="form-group" data-animate="fadeInUp" data-delay=".75"> <input class="form-control" type="email" name="EMAIL" placeholder="Enter Your E-mail Address" required></div>
                        <div class="subscribe-submit form-group clearfix mb-0" data-animate="fadeInUp" data-delay=".8"> <button class="btn btn-primary btn-square float-left" type="submit">Subscribe</button> <span>Don’t worry! <br>Your e-mail won’t be published.</span></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="row">
                <div class="col-md-5 order-last order-md-first">
                    <p class="copyright" data-animate="fadeInDown" data-delay=".85">&copy; Copyright 2018 <a href="javascript:void(0)">Tax Clinic</a></p>
                </div>
                <div class="col-md-7 order-first order-md-last">
                    <ul class="footer-menu list-inline text-md-right mb-md-0" data-animate="fadeInDown" data-delay=".95">
                        {{--<li><a href="javascript:void(0)">Afilliate</a></li>--}}
                        {{--<li>|</li>--}}
                        <li><a href="{{url('privacy_policy')}}">Privacy Policy</a></li>
                        <li>|</li>
                        <li><a href="{{url('terms_and_conditions')}}">Termns & Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="back-to-top"> <a href="#"> <i class="fas fa-arrow-up"></i></a></div>
@php

$blogs = \App\Blog::where('scheduled_status','1')->get();
foreach ($blogs as $blog){
    if (date('Y-m-d',strtotime($blog->publish_start_date)) == date('Y-m-d')){
        //dd('sameed');
        $singleBlog = \App\Blog::where('id',$blog->id)->update([
            'status' => '1'
        ]);
    }elseif(date('Y-m-d',strtotime($blog->publish_end_date)) < date('Y-m-d')){
        \App\Blog::where('id',$blog->id)->update([
            'status' => '0'
        ]);
    }
}
//dd($blog);

@endphp
<script>
    $(document).ready(function () {
        $('.mobile').mask("9999-9999999");
        $('.cnic').mask('99999-9999999-9');
        @if(session('contact_message'))

            $("#contact_modal .modal-body .contact_message").text("{{session('contact_message')}}");
            $("#contact_modal").modal();

        @endif

        @if(isset($mobile_verify))
            $('a').attr('href','javascript:void(0);')
        @endif

    });
</script>
{{--<script src="{{URL::asset('frontend')}}/js/taxcalculator.js"></script>--}}
<script src="{{URL::asset('frontend')}}/js/fontawesome-all.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/bootstrap.bundle.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/jquery.validate.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/typed.js/typed.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/waypoints/sticky.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/swiper/swiper.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/particles.js/particles.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/particles.js/particles.settings.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/parsley/parsley.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/parallax/parallax.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/color-switcher/color-switcher.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/retinajs/retina.min.js"></script>
{{--<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>--}}
<script src="{{URL::asset('frontend')}}/js/plugin.js"></script>
<script src="{{URL::asset('frontend')}}/js/menu.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/scripts.js"></script>
<script src="{{URL::asset('frontend')}}/js/custom.js"></script>
{{--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>--}}
{{--<script src="{{ asset('js/share.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>

</body>
</html>