@include('frontend.includes.header')
@php //dd($blogs); @endphp
      <section class="page-title-wrap position-relative bg-light">
         <div id="particles_js"></div>
         <div class="container">
            <div class="row">
               <div class="col-11">
                  <div class="page-title position-relative pt-5 pb-5">
                     <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Blog</a></li>
                     </ul>
                     <h1 data-animate="fadeInUp" data-delay="1.3">Blog</h1>
                  </div>
               </div>
               <div class="col-1">
                  <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
               </div>
            </div>
         </div>
      </section>
      <section class="blog pt-7 pb-7">
         <div class="container">
            <div class="row">
               <div class="col-md-8">

                  @if(!empty($blogs))
                     @foreach($blogs as $value)
                  <div class="single-post" data-animate="fadeInUp" data-delay=".1">
                     <div class="image-hover-wrap">
                        <img src="{{url('images/blog',$value->image_url)}}" alt="">
                        <div class="image-hover-content d-flex justify-content-center align-items-center text-center">
                           <ul class="list-inline">
                              <li><a href="#"><i class="fas fa-link"></i></a></li>
                              <li><a href="{{ url('single-blog',$value->id) }}"><i class="fas fa-share-alt"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     <span>Posted on <a href="#">{{ date('d-m-Y h:i A',strtotime($value->created_at)) }}</a></span>
                     <h4>{{ $value->title }}</h4>
                     <a href="{{ url('single-blog',$value->id) }}">Continue Reading<i class="fas fa-caret-right"></i></a>
                  </div>
                     @endforeach
                     {{$blogs->links()}}
                  @endif

                  {{--<ul class="custom-pagination list-inline text-center text-uppercase mt-4" data-animate="fadeInUp" data-delay=".1">--}}
                     {{--<li class="float-left disabled"><a href="#"><i class="fas fa-caret-left"></i> Prev</a></li>--}}
                     {{--<li class="active"><a href="#">01</a></li>--}}
                     {{--<li><a href="#">02</a></li>--}}
                     {{--<li><a href="#">03</a></li>--}}
                     {{--<li><a href="#">04</a></li>--}}
                     {{--<li><a href="#">05</a></li>--}}
                     {{--<li class="float-right"><a href="#">Next <i class="fas fa-caret-right"></i></a></li>--}}
                  {{--</ul>--}}
               </div>
               <div class="col-md-4">
                  <aside>
                     <div class="single-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 data-animate="fadeInUp" data-delay=".2">Recent posts</h3>
                        <ul class="recent-posts list-unstyled mb-0">
                            @if(!empty($limited_blog))
                                @foreach($limited_blog as $value)

                           <li data-animate="fadeInUp" data-delay=".25"><a href="{{ url('single-blog',$value->id) }}">{{ $value->title }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                     </div>
                     <div class="single-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 data-animate="fadeInUp" data-delay=".2">Tags</h3>
                        <ul class="tags roboto list-inline mb-0">
                           <li data-animate="fadeInUp" data-delay=".25"><a href="#">#Technology</a></li>
                           <li data-animate="fadeInUp" data-delay=".3"><a href="#">#Envato</a></li>
                           <li data-animate="fadeInUp" data-delay=".35"><a href="#">#ThemeForest</a></li>
                           <li data-animate="fadeInUp" data-delay=".4"><a href="#">#Domain</a></li>
                           <li data-animate="fadeInUp" data-delay=".45"><a href="#">#VPNet</a></li>
                           <li data-animate="fadeInUp" data-delay=".6"><a href="#">#CloudHostion</a></li>
                           <li data-animate="fadeInUp" data-delay=".65"><a href="#">#WordPress</a></li>
                        </ul>
                     </div>
                  </aside>
               </div>
            </div>
         </div>
      </section>
@include('frontend.includes.footer')