@include('frontend.includes.header')
<section class="page-title-wrap position-relative bg-light">
    <div id="particles_js"></div>
    <div class="container">
        <div class="row">
            <div class="col-11">
                <div class="page-title position-relative pt-5 pb-5">
                    <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url("/")}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Services</a></li>
                    </ul>
                    <h1 data-animate="fadeInUp" data-delay="1.3">Corporate</h1>
                </div>
            </div>
            <div class="col-1">
                <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="services-title position-relative pt-7" data-bg-img="{{URL::asset('frontend')}}/img/buildings.jpg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        {{--<h2 class="text-white" data-animate="fadeInUp" data-delay=".1">Why You Need Our Service</h2>--}}
                        {{--<p class="text-white" data-animate="fadeInUp" data-delay=".3">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-wrap bg-white position-relative pt-3 pb-3">
        <div class="container">
            <br>
            <p>Company limited by shares, guarantee or charitable / not for profit organizations registered with Securities and Exchange Commission of Pakistan “SECP”.
            </p>
            <p>There are several legal requirements in order to incorporate a company with SECP or in case of winding up of a company.
            </p>
            <p>Tax Clinic has all the experience and personnel with solid corporate background to handle all the matters professionally with SECP.
            </p>
            <p>Our team will keep in touch with you through our Online portal and resolve and advise all corporate matters.
            </p>
        </div>
    </div>
</section>
@include('frontend.includes.footer')