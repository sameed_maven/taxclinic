@include('frontend.includes.header')
      <section class="pt-7 pb-7">
         <div class="container">
             @if(session('message'))
                 <p class="alert alert-success">{{session('message')}}</p>
             @endif
            <div class="row align-items-lg-end col-md-4 col-md-offset-1" style="margin: 0 auto;">
               <h4>Please Verify Mobile Number</h4>
               <form action="{{url('/customer/mobile-confirm')}}" method="post" id="mobile_confirm" class="col-md-12" style="padding:0;">
                  {{csrf_field()}}
                  <div class="form-group" id="mobile_field" {{(session('no_mobile'))?'':'style=display:none;'}}>
                     <label for="confirm_mobile">Mobile Number</label>
                     <input name="mobile" id="mobile" onKeyPress="if(this.value.length==12) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control mobile">
                  </div>
                  <div class="form-group" id="mobile_verify_field" {{(session()->has('no_mobile'))?'style=display:none;':''}}>
                     <label for="confirm_mobile">Please Enter Your 4-Digit Mobile Verification Code</label>
                     <input type="number" name="confirm_mobile" onKeyPress="if(this.value.length==4) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control">
                     @if(session('error'))
                        <span class="help-block">
                           <strong>{{ session('error') }}</strong>
                        </span>
                     @endif
                  </div>
                  <div class="form-group">
                     <button type="submit" id="mobile_confirm_submit" class="btn btn-success">Confirm</button>
                     {{--<button type="button" id="change_mobile" class="btn btn-info">Change Mobile Number</button>--}}
                  </div>
               </form>
            </div>
         </div>
      </section>
@include('frontend.includes.footer')