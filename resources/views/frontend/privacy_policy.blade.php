@include('frontend.includes.header')
<section class="page-title-wrap position-relative bg-light">
    <div id="particles_js"></div>
    <div class="container">
        <div class="row">
            <div class="col-11">
                <div class="page-title position-relative pt-5 pb-5">
                    <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url("/")}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Details</a></li>
                    </ul>
                    <h1 data-animate="fadeInUp" data-delay="1.3">Privacy Policy</h1>
                </div>
            </div>
            <div class="col-1">
                <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="services-title position-relative pt-7" data-bg-img="{{URL::asset('frontend')}}/img/tax.jpg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        {{--<h2 class="text-white" data-animate="fadeInUp" data-delay=".1">Why You Need Our Service</h2>--}}
                        {{--<p class="text-white" data-animate="fadeInUp" data-delay=".3">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-wrap bg-white position-relative pt-5 pb-5">
        <div class="container">
            <br>
            <p>There is an exhaustive list of person who are required under the Income Tax Ordinance, 2001 “ITO” to file return of income through Online portal of FBR. The online process for registration of individual is simple, however, in order to incorporate the correct information about the particulars with respect to status such as Business/ Salaried/ Other, it is advised to get assistance from Professionals.
            </p>
            <p>Filing of Income Tax return is a very critical statement and the complete form of return to be filled with great care and incorporation financial information at correct designated fields.
            </p>
            <p>There are various rebates and tax credits available which an individual may miss out due to complexity of tax law and the conditions precedent attached to it.
            </p>
            <p>The assessment proceedings of a person enrolled with FBR is now on through e-portals and requires prompt response and submission of information and clarification through online portal only.
            </p>
            <p>Recently, amendment has been made that increase the incidence of tax to non-filer of tax return.
            </p>
            <p>Tax Clinic is providing professional services with ease of communication through online portal with our consultants available. We respond to your queries and submit your tax return professionally. You don’t need to visit any office, upload / share information through our secure portal with respect to your income, tax and wealth etc, our consultants prepare the computation and shared with you before they submit the same to FBR. Our consultants remain vigilant for any notices etc once you remain enrolled with us. Moreover, relevant updates will be shared with you directly/indirectly affecting you.
            </p>
            <p>In case of any notice for explanation/audit raised, you share the information with us through online portal and our consultants handle you case professionally.
            </p>
            <p>In case you require any personal appointment and visit our Clinic you are always welcome on prior appointment.
            </p>
            {{--<div class="roboto text-center font-weight-medium" data-animate="fadeInUp" data-delay=".65">--}}
                {{--<p>If you have any questions in your mind, Just <a href="contact.html">click here</a> to write or you can <br>Call to <a href="tel:1234567890">(+1) 234-567-890</a></p>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
@include('frontend.includes.footer')