@include('frontend.includes.header')
<section class="page-title-wrap position-relative bg-light">
    <div id="particles_js"></div>
    <div class="container">
        <div class="row">
            <div class="col-11">
                <div class="page-title position-relative pt-5 pb-5">
                    <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url("/")}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Services</a></li>
                    </ul>
                    <h1 data-animate="fadeInUp" data-delay="1.3">Sales Tax</h1>
                </div>
            </div>
            <div class="col-1">
                <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="services-title position-relative pt-7" data-bg-img="{{URL::asset('frontend')}}/img/buildings.jpg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        {{--<h2 class="text-white" data-animate="fadeInUp" data-delay=".1">Why You Need Our Service</h2>--}}
                        {{--<p class="text-white" data-animate="fadeInUp" data-delay=".3">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-wrap bg-white position-relative pt-3 pb-3">
        <div class="container">
            <br>

            <p>Our Sales Tax Act, 1990 “STA” is a hybrid of General Sales Tax and Value Added Tax, over the years various amendments made in the law and interpretation of various of provisions is governed through judgement of appellate authorities.
            </p>
            <p>There are many businesses covered under the special procedures defined by the STA and charge of ST on various goods through SRO which makes it difficult for a common person to interpret which leads them to complex position.
            </p>
            <p>At Tax Clinic, our consultants look into these matters for you through online portal and suggest / guide on these complex issues with solutions.
            </p>
            <p>We also help you to file your monthly sales tax return and also represent you in case of any query raised by FBR or Audit proceedings initiated by them.
            </p>
            <p>In case of any ST litigation we will file appeal to appropriate forum to obtain relief from undue tax burden.
            </p>
            {{--<div class="roboto text-center font-weight-medium" data-animate="fadeInUp" data-delay=".65">--}}
            {{--<p>If you have any questions in your mind, Just <a href="contact.html">click here</a> to write or you can <br>Call to <a href="tel:1234567890">(+1) 234-567-890</a></p>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
@include('frontend.includes.footer')