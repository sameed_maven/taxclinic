@include('includes.header')
<section class="pt-2 pb-2">
    <div class="container">
        <div class="row align-items-lg-end">
            <div class="col-md-12">
                <h3 data-animate="fadeInUp" data-delay=".1" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.1s;">Add blog</h3>
                <form action="{{url('/'.Request::segment(1).'/blog/edit',$blog->id)}}" method="post" name="blog" id="blog" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Title</label>
                                    <input type="text" id="title" name="title" class="form-control" value="{{(old('title')!='')?old('title'):$blog->title}}">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row p-t-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Tags</label> <br>
                                    <button type="button" data-toggle="modal" data-target="#tag"  class="btn btn-info">Add Tag</button>
                                    <div id="selected_tag_input">
                                        @foreach($blog_tag as $tags)
                                            @php //dd($tag->tag->name); @endphp
                                            <input type="hidden" id="{{$tags->tag->name}}" name="tags[]" value="{{$tags->tag->name}}">
                                        @endforeach
                                    </div>
                                </div>
                                <div id="selected_tags"  class="form-group">
                                    @foreach($blog_tag as $tags)
                                    <div class="alert alert-info btn-sm final_value" style="width: 20%; float: left; margin-left: 10px;">
                                        {{$tags->tag->name}}
                                        <button type="button" id="{{$tags->tag->name}}" class="close btn-xs remove_tag" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row p-t-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Blog Content</label>
                                    <textarea name="description" id="description" class="form-control">{{(old('description'))?old('description'):$blog->content}}</textarea>
                                    <br>
                                    {{--<p class="text-right">(Max Words 1000)</p>--}}

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Image</label>
                                    <input type="file" name="post_image" class="form-control"aria-invalid="false">
                                    @if ($errors->has('post_image'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('post_image') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <img src="{{url('images/blog',$blog->image_url)}}" class="img img-responsive" alt="">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row p-t-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Schedule</label> <br>
                                    <button type="button" id="schedule_btn" class="btn btn-info">Schedule</button>
                                    <input type="hidden" name="publish_start_date" value="{{$blog->publish_start_date}}">
                                    <input type="hidden" name="publish_end_date" value="{{$blog->publish_end_date}}">
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <input type="hidden" name="status" value="{{$blog->status}}">
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success" id="blog_submit"> <i class="fa fa-check"></i> Save</button>
                        <a href="{{url('/'.Request::segment(1).'/blog')}}" class="btn btn-info">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<div id="tag" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Tag</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row p-t-20" id="new_tag_div" style="display: none;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="new_tags">Add New Tag</label>
                            <input type="text" class="form-control" name="new_tags" id="new_tags_hide">
                        </div>
                    </div>
                </div>
                <div class="row p-t-20" id="exis_tag_div">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exis_tag">Use Existing Tags</label>
                            <select name="exis_tags" id="exis_tags" class="form-control select-picker exis_tags">
                                <option value="">Select Tag</option>
                                @foreach($tag as $tag)
                                    <option value="{{$tag->name}}">{{$tag->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-xs btn-info" id="new_tag">Add New Tag</button>
                <div id="select_tag"></div>
                <div id="input_tag"></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="add_tag" class="btn btn-info waves-effect">Add</button>
                <button type="button" class="btn btn-info waves-effect" id="add_tag_front">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div id="schedule" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Schedule to Publish Your Blog</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <p class="error"></p>
                <form action="#" id="publish_date">
                    <div class="row p-t-20" id="">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="new_tags">Publish Start Date</label>
                                <input class="form-control datepicker" name="start_date" id="start_date" value="{{$blog->publish_start_date}}">
                            </div>
                        </div>
                    </div>
                    <div class="row p-t-20" id="">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="new_tags">Publish End Date</label>
                                <input class="form-control datepicker" name="end_date" id="end_date" value="{{$blog->publish_end_date}}">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="schedule_blog" class="btn btn-info waves-effect">Schedule</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@include('includes.footer')