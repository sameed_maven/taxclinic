@include('includes.header')
<section class="pt-2 pb-2 admin_section">
    <div class="container">
        <div class="row align-items-lg-end">
            <div class="col-md-12">
                @if(session('message'))
                    {!! session('message') !!}
                @endif
                @if(session('error'))
                        <p class="alert alert-danger">{!! session('error') !!}</p>
                @endif
                <h3 data-animate="fadeInUp" data-delay=".1" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.1s;">Customers List</h3>
                    <h4 class="card-title"  style="float: right;">
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn waves-effect waves-light btn-primary model_img img-responsive">Import</button></h4>
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Mobile Number</th>
                        <th>CNIC</th>
                        <th>Email</th>
                        <th>SMS</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if(isset($customer))
                        @foreach($customer as $customer)
                            {{--@php dd($customer[0]); @endphp--}}

                        <tr>
                            <td>{{decrypt($customer[0]['first_name']).' '.decrypt($customer[0]['last_name'])}}</td>
                            <td>{{$customer[0]['user']['email']}}</td>
                            <td>{{decrypt($customer[0]['mobile_number'])}}</td>
                            <td>{{$customer[0]['cnic']}}</td>
                            <td><span data-toggle="tooltip" title="Date:29-05-18" class="fa {{($customer[0]['email_status']==1)?'fa-check':'fa-times-circle'}} mail_send"></span></td>
                            <td><span class="fa {{($customer[0]['sms_status']==1)?'fa-check':'fa-times-circle'}} not_send"></span></td>
                            <td>
                                <a href="{{url('/admin/agent/'.Request::segment(3).'/customer/edit',$customer[0]['id'])}}"  class="btn waves-effect waves-light btn-xs btn-danger" >Edit</a>
                                <button type="button" class="btn waves-effect waves-light btn-xs btn-info email_send" data-id="{{$customer[0]['user']['id']}}" data-email="{{$customer[0]['user']['email']}}">Send Email</button>
                                <button type="button" class="btn waves-effect waves-light btn-xs btn-info sms_send" data-id="{{$customer[0]['user']['id']}}" data-mobile="{{decrypt($customer[0]['mobile_number'])}}">Send SMS</button>
                            </td>
                        </tr>
                        @endforeach
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload CSV File</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{url('/admin/agent/'.Request::segment(3).'/customer/import')}}" method="post" name="csv_import" id="csv_import" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="dropify-wrapper">
                    {{csrf_field()}}
                    <input type="file" id="input-file-max-fs" name="imported_file" class="btn waves-effect waves-light btn-primary" data-max-file-size="2M">


                </div>
                <div class="dropify-loader"></div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info waves-effect" id="csv_import_submit" >Upload</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.SMS -->
<div id="sms_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Send SMS</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{url('/admin/customer/send-sms')}}" id="send_sms" method="post">
                {{csrf_field()}}
            <div class="modal-body">
                <div class="row p-t-20">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" name="user_id">
                            <input type="hidden" name="mobile">
                            <label for="new_tag">SMS</label>
                            <textarea name="message" id="message" cols="30" rows="3" placeholder="Enter Your Message" class="form-control"></textarea>
                            <p class="text-right">(Max letters 60)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info waves-effect" id="send_sms_submit">Send</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--EMail-->
<div id="email_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Send Email</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{url('/admin/customer/send-email')}}" method="post" id="send_email">
                {{csrf_field()}}
            <div class="modal-body">
                <div class="row p-t-20">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" name="user_id">
                            <input type="hidden" name="email">
                            <label for="new_tag">Email</label>
                            <textarea name="message" id="message" cols="30" rows="3" placeholder="Enter Your Message" class="form-control"></textarea>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="send_email_submit" class="btn btn-info waves-effect">Send</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    // $(document).ready(function(){
    //     $('[data-toggle="tooltip"]').tooltip();
    // });
</script>
@include('includes.footer')