@include('includes.header')
<section class="pt-2 pb-2 admin_section">
    <div class="container">
        <div class="row align-items-lg-end">
            <div class="col-md-12">
                @if(session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                @endif
                <h3 data-animate="fadeInUp" data-delay=".1" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.1s;">Agent List</h3>
                    <h4 class="card-title" style="float: right;">
                        <a href="{{url('admin/agent/add')}}" class="btn waves-effect waves-light btn-primary">Add Agent</a>
                    </h4>
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile Number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($agent as $agent)
                        @php //dd($agent); @endphp
                        <tr>
                            <td>{{$agent->first_name.' '.$agent->last_name}}</td>
                            <td>{{$agent->user->email}}</td>
                            <td>{{$agent->mobile}}</td>
                            <td>
                                <a href="{{url('/admin/agent/edit',$agent->id)}}"  class="btn waves-effect waves-light btn-xs btn-danger" >Edit</a>
                                <a  href="{{url('/admin/agent/'.$agent->id.'/customers')}}" class="btn waves-effect waves-light btn-xs btn-info">Customers</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@include('includes.footer')