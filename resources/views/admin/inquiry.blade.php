@include('includes.header')
    <section class="pt-2 pb-2 admin_section">
        <div class="container">
            <div class="row align-items-lg-end">
                <div class="col-md-12">
                    @if(session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                    @endif
                    <h3 data-animate="fadeInUp" data-delay=".1" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.1s;">Inquiry</h3>
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile Number</th>
                            <th>Massege</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($inquiry as $inquirys)
                            <tr>
                                <td>{{$inquirys->first_name.' '.$inquirys->last_name}}</td>
                                <td>{{$inquirys->user->email}}</td>
                                <td>{{$inquirys->mobile}}</td>
                                <td>{{ substr($inquirys->message, 0, 40)}}</td>
                                <td>
                                    <a href="{{url('/admin/inquiry/message',$inquirys->id)}}"  class="btn waves-effect waves-light btn-xs btn-danger" >View</a>
                                    {{--<button type="button" class="btn waves-effect waves-light btn-xs btn-info">Create Customer</button>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
{{--                    {!! $inquiry->links() !!}--}}
                </div>
            </div>
        </div>
    </section>

    <div id="view_message" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Message</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100px;"><div class="chat-box" style="overflow: hidden; width: auto; height: 470px;">
                                    <!--chat Row -->
                                    <ul class="chat-list">
                                        <!--chat Row -->
                                        <li>
                                            <!--                                    <div class="chat-img"><img src="../assets/images/users/1.jpg" alt="user"></div>-->
                                            <div class="chat-content">
                                                <h5>Tiger Nixon</h5>
                                                <div class="box bg-light-info message1">Hi! My Name is Abdul Sami Khan</div>
                                            </div>
                                            <div class="chat-time">10:56 am</div>
                                        </li>

                                    </ul>
                                </div><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 154px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 316.93px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                        </div>
                        <div class="card-body b-t">
                            <div class="row">
                                <div class="col-8">
                                    <textarea placeholder="Type your message here" class="form-control b-0"></textarea>
                                </div>
                                <div class="col-4 text-right">
                                    <button type="button" class="btn btn-info btn-circle btn-lg"><i class="fa fa-paper-plane-o"></i> </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@include('includes.footer')