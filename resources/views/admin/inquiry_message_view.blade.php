@include('includes.header')
<section class="pt-2 pb-2">
    <div class="container">
        <div class="row align-items-lg-end">
            <div class="col-md-12">
                <div class="post-details animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="animation-duration: 0.6s; animation-delay: 0.1s;">
                    <div class="comments">
                        <h3 data-animate="fadeInUp" data-delay=".1" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.1s;">Inquiry Message View</h3>
                        <ul class="main-comment list-unstyled">
                            @foreach($message as $message)
                            <li data-animate="fadeInUp" data-delay=".2" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.2s;">
                                <div class="single-comment">
                                    <!-- <div class="comment-author float-left"> <img src="img/authors/comment-author1.png" alt=""></div> -->
                                    <div class="comment-content">
                                        <h4>{{($message->user_id)?$message->first_name.' '.$message->last_name:'Admin'}}</h4>
                                        <span>{{date('d-m-Y h:i A',strtotime($message->created_at))}}</span>
                                        <p>{{$message->message}}</p>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="comment-form mt-5">
                        <form action="{{url('admin/inquiry/message',Request::segment(4))}}" method="post" name="inuiry_reply" class="form-group col-md-12" id="inquiry_reply">
                            {{csrf_field()}}
                            <input type="hidden" name="inquiry_id" value="{{$inquiry_id->id}}">
                            <textarea name="inquiry_message" id="inquiry_message" class="form-control animated fadeInUp" placeholder="Write your Message" data-animate="fadeInUp" data-delay=".2" style="animation-duration: 0.6s; animation-delay: 0.2s;">{{old('inquiry_message')}}</textarea>
                            @if ($errors->has('inquiry_message'))
                                <span class="help-block">
                            <strong>{{ $errors->first('inquiry_message') }}</strong>
                            </span>
                            @endif
                            <button type="submit" class="btn btn-square btn-primary mt-3 animated fadeInUp" id="inquiry_reply_submit" data-animate="fadeInUp" data-delay=".6" style="animation-duration: 0.6s; animation-delay: 0.6s;">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('includes.footer')