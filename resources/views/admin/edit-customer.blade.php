@include('includes.header')
<section class="pt-2 pb-2">
    <div class="container">
        <div class="row align-items-lg-end">
            <div class="col-md-12">
                <h3 data-animate="fadeInUp" data-delay=".1" class="animated fadeInUp" style="animation-duration: 0.6s; animation-delay: 0.1s;">Edit Customer</h3>
                <form action="{{url('/admin/agent/'.Request::segment(3).'/customer/edit',$customer->id)}}" method="post" name="customer" id="customer" enctype="multipart/form-data">
                    {{csrf_field()}}
                    {{--@php dd($customer);@endphp--}}
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <input type="text" id="fname" name="fname" class="form-control" value="{{decrypt($customer->first_name)}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" id="lname" name="lname" class="form-control" value="{{decrypt($customer->last_name)}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="email" id="email" name="email" readonly class="form-control" value="{{$customer->user->email}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Mobile Number</label>
                                    <input onKeyPress="if(this.value.length==12) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" id="mnum" name="mnum" class="form-control mobile" value="{{decrypt($customer->mobile_number)}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Phone Number</label>
                                    <input onKeyPress="if(this.value.length==12) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" id="pnum" name="pnum" class="form-control mobile" value="{{decrypt($customer->phone_number)}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <input type="text" id="address" name="address" class="form-control" value="{{decrypt($customer->address)}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Country</label>
                                    <input type="text" id="country" name="country" class="form-control" value="{{decrypt($customer->country)}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">CNIC</label>
                                    <input onKeyPress="if(this.value.length==14) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" id="cnic" name="cnic" class="form-control cnic" value="{{$customer->cnic}}">
                                </div>
                            </div>

                        </div>

                        {{--<div class="row p-t-20">--}}
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Address</label>--}}
                                    {{--<input type="text" id="address" name="address" class="form-control" value="{{$agent->address}}">--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">City</label>--}}
                                    {{--<input type="text" id="city" name="city" class="form-control" value="{{$agent->city}}">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Area</label>--}}
                                    {{--<input type="text" id="area" name="area" class="form-control" value="{{$agent->area}}">--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Comapany</label>--}}
                                    {{--<input type="text" id="company" name="company" class="form-control" value="{{$agent->company}}">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}


                        <!--/row-->
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success" id="customer_submit"> <i class="fa fa-check"></i> Save</button>
                        <a href="{{url('admin/agent')}}" class="btn btn-info">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@include('includes.footer')