@include('includes.header')
      <section class="page-title-wrap position-relative bg-light">
         <div id="particles_js"></div>
         <div class="container">
            <div class="row">
               <div class="col-11">
                  <div class="page-title position-relative pt-5 pb-5">
                     <ul class="custom-breadcrumb roboto list-unstyled mb-0 clearfix" data-animate="fadeInUp" data-delay="1.2">
                        <li><a href="{{url('/'.Request::segment(1).'/dashboard')}}">Home</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="{{url('/'.Request::segment(1).'/blog')}}">Blogs</a></li>
                        <li><i class="fas fa-angle-double-right"></i></li>
                        <li><a href="#">Blog Preview</a></li>
                     </ul>
                     <h1 data-animate="fadeInUp" data-delay="1.3">Blog Preview</h1>
                  </div>
               </div>
               <div class="col-1">
                  <div class="world-map position-relative"> <img src="{{URL::asset('frontend')}}/img/map.svg" alt="" data-no-retina class="svg"></div>
               </div>
            </div>
         </div>
      </section>
      <section class="blog pt-2 pb-2">
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="post-details" data-animate="fadeInUp" data-delay=".1">
                     <div class="post-content">
                        <img src="{{url('images/blog',$singleblog->image_url)}}" alt="" data-animate="fadeInUp" data-delay=".2"> <span data-animate="fadeInUp" data-delay=".3">Posted on <a href="#">{{ date('d-m-Y h:i A',strtotime($singleblog->created_at)) }}</a> By <a href="#">Nettie G. Sargent</a></span>
                        <h2 data-animate="fadeInUp" data-delay=".4">{{ $singleblog->title }}</h2>
                        {!!$singleblog->content!!}
                     </div>
                     <div class="row align-items-center half-gutters mb-5 tag-and-share">
                        <div class="col-xl-7 col-lg-6">
                           <ul class="tags roboto list-inline mb-lg-0 mb-md-3">
                              <li data-animate="fadeInUp" data-delay=".1"><i class="fas fa-tags"></i></li>
                              @foreach($blog_tag as $blogTag)
                                 <li data-animate="fadeInUp" data-delay=".15"><a href="#">#{{$blogTag->tag->name}}</a></li>
                              @endforeach
                           </ul>
                        </div>
                        <div class="col-xl-5 col-lg-6">
                           <ul class="social-share list-inline mb-0 text-lg-right">
                              <li data-animate="fadeInUp" data-delay=".5"><a class="linkedin" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                              <li data-animate="fadeInUp" data-delay=".55"><a class="google" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                              <li data-animate="fadeInUp" data-delay=".6"><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                              <li data-animate="fadeInUp" data-delay=".65"><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <aside>
                     {{--<div class="single-widget" data-animate="fadeInUp" data-delay=".1">--}}
                        {{--<form action="#">--}}
                           {{--<div class="form-group position-relative mb-0"> <input class="form-control" type="text" placeholder="Search" data-parsley-required-message="Please type at least one word." data-parsley-minlength="3" data-parsley-minlength-message="Please type at least one word." required> <button type="submit"><i class="fas fa-search"></i></button></div>--}}
                        {{--</form>--}}
                     {{--</div>--}}

                     <div class="single-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 data-animate="fadeInUp" data-delay=".2">Recent posts</h3>
                        <ul class="recent-posts list-unstyled mb-0">
                           <li data-animate="fadeInUp" data-delay=".25"><a href="#">How to Watch Smith VS Holzken Live Online From Anywhere?</a></li>
                           <li data-animate="fadeInUp" data-delay=".3"><a href="#">In Major Hiring Push, Web Hosting Powerhouse Go Daddy to Expand</a></li>
                           <li data-animate="fadeInUp" data-delay=".35"><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit fastus.</a></li>
                           <li data-animate="fadeInUp" data-delay=".4"><a href="#">How to Watch Emmett VS Stephens at UFC Fight Night FOX 28?</a></li>
                           <li data-animate="fadeInUp" data-delay=".45"><a href="#">UK children being subjected to invasive data retention</a></li>
                        </ul>
                     </div>
                     {{--<div class="single-widget" data-animate="fadeInUp" data-delay=".1">--}}
                        {{--<h3 data-animate="fadeInUp" data-delay=".2">Follow Us</h3>--}}
                        {{--<ul class="row half-gutters follow-us list-unstyled">--}}
                           {{--<li class="col-4" data-animate="fadeInUp" data-delay=".25"> <a class="facebook" href="#"> <i class="fab fa-facebook-f"></i> <span>Like 23.5k</span> </a></li>--}}
                           {{--<li class="col-4" data-animate="fadeInUp" data-delay=".3"> <a class="twitter" href="#"> <i class="fab fa-twitter"></i> <span>Follow 4.5k</span> </a></li>--}}
                           {{--<li class="col-4" data-animate="fadeInUp" data-delay=".35"> <a class="google" href="#"> <i class="fab fa-google-plus-g"></i> <span>Like 82.5k</span> </a></li>--}}
                           {{--<li class="col-4" data-animate="fadeInUp" data-delay=".4"> <a class="pinterest" href="#"> <i class="fab fa-pinterest-p"></i> <span>Follow 6.9k</span> </a></li>--}}
                           {{--<li class="col-4" data-animate="fadeInUp" data-delay=".45"> <a class="rss" href="#"> <i class="fas fa-rss"></i> <span>follow 2.8k</span> </a></li>--}}
                           {{--<li class="col-4" data-animate="fadeInUp" data-delay=".5"> <a class="linkedin" href="#"> <i class="fab fa-linkedin-in"></i> <span>follow 3.5k</span> </a></li>--}}
                        {{--</ul>--}}
                     {{--</div>--}}
                     <div class="single-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 data-animate="fadeInUp" data-delay=".2">Tags</h3>
                        <ul class="tags roboto list-inline mb-0">
                           <li data-animate="fadeInUp" data-delay=".25"><a href="#">#Technology</a></li>
                           <li data-animate="fadeInUp" data-delay=".3"><a href="#">#Envato</a></li>
                           <li data-animate="fadeInUp" data-delay=".35"><a href="#">#ThemeForest</a></li>
                           <li data-animate="fadeInUp" data-delay=".4"><a href="#">#Domain</a></li>
                           <li data-animate="fadeInUp" data-delay=".45"><a href="#">#VPNet</a></li>
                           <li data-animate="fadeInUp" data-delay=".6"><a href="#">#CloudHostion</a></li>
                           <li data-animate="fadeInUp" data-delay=".65"><a href="#">#WordPress</a></li>
                        </ul>
                     </div>
                  </aside>
               </div>
            </div>
         </div>
      </section>
@include('includes.footer')