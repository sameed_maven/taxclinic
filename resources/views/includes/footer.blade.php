<footer class="main-footer bg-primary">
    <div class="container">

        <div class="bottom-footer">
            <div class="row">
                <div class="col-md-5 order-last order-md-first">
                    <p class="copyright" data-animate="fadeInDown" data-delay=".85">&copy; Copyright 2018 <a href="javascript:void(0)">Tax Clinic</a></p>
                </div>
                <div class="col-md-7 order-first order-md-last">
                    <ul class="footer-menu list-inline text-md-right mb-md-0" data-animate="fadeInDown" data-delay=".95">
                        {{--<li><a href="javascript:void(0)">Afilliate</a></li>--}}
                        {{--<li>|</li>--}}
                        <li><a href="{{url('privacy_policy')}}">Privacy Policy</a></li>
                        <li>|</li>
                        <li><a href="{{url('terms_and_conditions')}}">Termns & Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="back-to-top"> <a href="#"> <i class="fas fa-arrow-up"></i></a></div>
<script src="{{URL::asset('frontend')}}/js/jquery-3.2.1.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/fontawesome-all.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/bootstrap.bundle.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/jquery.validate.js"></script>
<script src="{{URL::asset('frontend')}}/js/additional-method.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/typed.js/typed.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/waypoints/sticky.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/swiper/swiper.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/particles.js/particles.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/particles.js/particles.settings.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/parsley/parsley.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/parallax/parallax.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/color-switcher/color-switcher.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/retinajs/retina.min.js"></script>
<script src="{{URL::asset('frontend')}}/plugins/ckeditor/ckeditor.js"></script>
<script src="{{URL::asset('frontend')}}/js/bootstrap-datepicker.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/menu.min.js"></script>
<script src="{{URL::asset('frontend')}}/js/scripts.js"></script>
<script src="{{URL::asset('frontend')}}/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>
<script>
    $(document).ready(function () {
       $('.datepicker').datepicker({
           format:'yyyy-mm-dd'
       });
    });
</script>
</body>
</html>