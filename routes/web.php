<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


// =====================================================
// Frontend Section ====================================
// =====================================================
Route::group(array('prefix'=>''),function(){
    Route::get('/', 'FrontendController@home');
    Route::get('/contact','FrontendController@contact');
    Route::post('/contact',"FrontendController@contact_mail");
    Route::get('/inquiry','FrontendController@inquiry_form');
    Route::get('/free-tax-cal', 'FrontendController@free_tax_cal');
    Route::post('/free-tax-cal', 'FrontendController@free_tax_cal_submit');
    Route::post('/inquiry',"FrontendController@inquiry_mail");
    Route::post('/customer_register',"FrontendController@create_customer");
    Route::get('/blog','BlogController@show');
    Route::get('/income-tax','FrontendController@income_tax');
    Route::get('/single-blog/{id}','BlogController@singleblog');
    Route::get('/sales-tax','FrontendController@sales_tax');
    Route::get('/provincial-tax','FrontendController@provincial_tax');
    Route::get('/corporate','FrontendController@corporate');
    Route::get('/business-setup','FrontendController@business_setup');
    Route::get('/redirect_fb', 'SocialAuthFacebookController@redirect');
    Route::get('/callback_fb', 'SocialAuthFacebookController@callback');
    Route::get('/redirect_tw', 'SocialAuthTwitterController@redirect');
    Route::get('/callback_tw', 'SocialAuthTwitterController@callback');
    Route::get('/redirect_ln', 'SocialAuthLinkedinController@redirect');
    Route::get('/callback_ln', 'SocialAuthLinkedinController@callback');
    Route::get('/redirect_gl', 'SocialAuthGoogleController@redirect');
    Route::get('/callback_gl', 'SocialAuthGoogleController@callback');

    Route::get('/migrate',function (){
        return Artisan::call('migrate');
    });

    Route::get('/clear_config',function (){
        return Artisan::call('config:clear');
    });

    Route::get('/clear_view',function (){
        return Artisan::call('view:clear');
    });

    Route::get('/clear_cache',function (){
        return Artisan::call('cache:clear');
    });

    Route::get('/queue',function (){
        return Artisan::call('queue:work');
    });

    Route::get('/autoload', function()
    {
        exec('composer dump-autoload');
        echo 'composer dump-autoload complete';
    });

    Route::get('/update', function()
    {
        exec('composer update');
        echo 'composer update complete';
    });

    Route::get('/terms_and_conditions','FrontendController@tc');

    Route::get('/privacy_policy','FrontendController@pp');

    Route::get('/email_confirm','FrontendController@email_confirm');
    Route::get('/smsapi','FrontendController@smsapi');
});

// =====================================================
// Admin Section ====================================
// =====================================================
Route::group(array('prefix'=>'admin','middleware' => ['auth','role:admin']), function (){
    Route::get('/dashboard', 'HomeController@admin');
    Route::get('/inquiry','InquiryController@index');
    Route::get('/inquiry/auto','InquiryController@create');
    Route::get('/inquiry/message/{id}','InquiryController@inquiry_message')->where('id', '[0-9]+');
    Route::post('/inquiry/message/{id}','InquiryController@save_message')->where('id', '[0-9]+');
    Route::get('/blog','BlogController@index');
    Route::get('/blog/add','BlogController@create');
    Route::post('/blog/add','BlogController@store');
    Route::get('/blog/publish/{id}','BlogController@publish');
    Route::get('/blog/unpublish/{id}','BlogController@unpublish');
    Route::get('/blog/edit/{id}','BlogController@edit');
    Route::post('/blog/edit/{id}','BlogController@update');
    Route::get('/blog/preview/{id}','BlogController@blog_preview');
    Route::get('/agent','AgentController@index');
    Route::get('/agent/add','AgentController@create');
    Route::post('/agent/add','AgentController@store');
    Route::get('/agent/edit/{id}','AgentController@edit');
    Route::post('/agent/edit/{id}','AgentController@update');
    Route::get('/agent/{id}/customers','CustomerController@index');
//    Route::get('customers', 'CustomerController@index');
    Route::post('/agent/{id}/customer/import', 'CustomerController@import');
    Route::get('/agent/{id}/customer/edit/{cus_id}','CustomerController@edit');
    Route::post('/agent/{id}/customer/edit/{cus_id}','CustomerController@update');
    Route::post('/customer/send-email','CustomerController@send_email');
    Route::post('/customer/send-sms','CustomerController@sms_send');
});

// =====================================================
// Agent Section ====================================
// =====================================================
Route::group(array('prefix'=>'agent','middleware' => ['auth','role:agent']), function (){
    Route::get('/dashboard', 'HomeController@agent');
});

// =====================================================
// Consultant Section ====================================
// =====================================================
Route::group(array('prefix'=>'consultant','middleware' => ['auth','role:consultant']), function (){
//    Route::get('/dashboard', 'HomeController@agent');
});

// =====================================================
// Customer Section ====================================
// =====================================================
Route::group(array('prefix'=>'customer','middleware' => ['auth','role:customer']), function (){
    Route::get('/dashboard', 'HomeController@customer');
    Route::get('/mobile-confirm','CustomerController@confirm_mobile');
    Route::post('/mobile-confirm','CustomerController@mobile_confirm');
});
