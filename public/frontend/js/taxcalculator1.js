function onPageLoad() {

    document['getElementById']('salary')['focus']();
    var _0x9f49x2 = document['getElementById']('salary');
    _0x9f49x2['onkeydown'] = inputUpdated;
    _0x9f49x2['onpaste'] = inputUpdated;
    _0x9f49x2['oninput'] = inputUpdated;
    _0x9f49x2['onkeypress'] = inputUpdated;
    document['getElementById']('calculate-tax')['scrollIntoView']()
}

function inputUpdated(_0x9f49x4) {

    var _0x9f49x5 = (_0x9f49x4['charCode'] >= 48 && _0x9f49x4['charCode'] <= 57) || _0x9f49x4['charCode'] == 0;
    var _0x9f49x2 = document['getElementById']('salary')['value'];
    if (_0x9f49x2 == '') {
        _0x9f49x2 = '0'
    };

    var _0x9f49x6 = calculateTax(_0x9f49x2 * 12);
    document['getElementById']('monthly-salary')['value'] = _0x9f49x2;
    document['getElementById']('monthly-tax')['value'] = round(_0x9f49x6 / 12);
    document['getElementById']('salary-after-tax')['value'] = (_0x9f49x2 - round(_0x9f49x6 / 12));
    document['getElementById']('yearly-salary')['value'] = round(_0x9f49x2 * 12);
    document['getElementById']('yearly-income-after-tax')['value'] = ((_0x9f49x2 * 12) - _0x9f49x6);
    document['getElementById']('yearly-tax')['value'] = _0x9f49x6;
    return _0x9f49x5
}

function round(_0x9f49x8) {
    return +(Math['round'](_0x9f49x8 + 'e+2') + 'e-2')
}

function calculateTax(_0x9f49xa) {

    var _0x9f49x6 = 0;
    if (_0x9f49xa > 400000 && _0x9f49xa <= 500000) {
        _0x9f49xa -= 400000;
        _0x9f49x6 = _0x9f49xa * 0.02
    } else {
        if (_0x9f49xa > 500000 && _0x9f49xa <= 750000) {
            _0x9f49xa -= 500000;
            _0x9f49x6 = 2000 + _0x9f49xa * 0.05
        } else {
            if (_0x9f49xa > 750000 && _0x9f49xa <= 1400000) {
                _0x9f49xa -= 750000;
                _0x9f49x6 = 14500 + _0x9f49xa * 0.1
            } else {
                if (_0x9f49xa > 1400000 && _0x9f49xa <= 1500000) {
                    _0x9f49xa -= 1400000;
                    _0x9f49x6 = 79500 + _0x9f49xa * 0.125
                } else {
                    if (_0x9f49xa > 1500000 && _0x9f49xa <= 1800000) {
                        _0x9f49xa -= 1500000;
                        _0x9f49x6 = 92000 + _0x9f49xa * 0.15
                    } else {
                        if (_0x9f49xa > 1800000 && _0x9f49xa <= 2500000) {
                            _0x9f49xa -= 1800000;
                            _0x9f49x6 = 137000 + _0x9f49xa * 0.175
                        } else {
                            if (_0x9f49xa > 2500000 && _0x9f49xa <= 3000000) {
                                _0x9f49xa -= 2500000;
                                _0x9f49x6 = 259500 + _0x9f49xa * 0.2
                            } else {
                                if (_0x9f49xa > 3000000 && _0x9f49xa <= 3500000) {
                                    _0x9f49xa -= 3000000;
                                    _0x9f49x6 = 359000 + _0x9f49xa * 0.225
                                } else {
                                    if (_0x9f49xa > 3500000 && _0x9f49xa <= 4000000) {
                                        _0x9f49xa -= 3500000;
                                        _0x9f49x6 = 472000 + _0x9f49xa * 0.25
                                    } else {
                                        if (_0x9f49xa > 4000000 && _0x9f49xa <= 7000000) {
                                            _0x9f49xa -= 4000000;
                                            _0x9f49x6 = 597000 + _0x9f49xa * 0.275
                                        } else {
                                            if (_0x9f49xa > 7000000) {
                                                _0x9f49xa -= 7000000;
                                                _0x9f49x6 = 1422000 + _0x9f49xa * 0.3
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    return round(_0x9f49x6)
}