/*==================================================================================
    Custom JS (Any custom js code you want to apply should be defined here).
====================================================================================*/

$(function(){
    $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(500);
    });

    $(".alert-danger").fadeTo(10000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(500);
    });



    $("#inquiry_reply").validate({
        focusInvalid: false,
        rules: {
            inquiry_message: "required"
        },
        message: {
            inquiry_message: "This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });
    $.validator.addMethod("number_pattern", function(value, element, patternpr) {
        return patternpr.test(value);
    },"Invalid Pattern Number Allow only");

    $("#contact_form").validate({
        focusInvalid: false,
        rules: {
            name: "required",
            email: {
                required: true,
                email:true
            },
            telephone:{
                required:true,
                number_pattern: /^[0-9 -]+$/,
                maxlength:11,
            },
            message: "required"
        },
        message: {
            name: "This field is required",
            email:{
                required:"This field is required",
                email:"This Email is not Valid"
            },
            telephone:{
                required:"This field is required",
                number_pattern:"Only Digits Numers Allow",
                maxlength:"Maximum Length of Numbers is 12"
            },
            message:"This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $("#agent").validate({
        focusInvalid: false,
        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email:true
            },
            mnum:{
                required:true,
                number_pattern: /^[0-9 -]+$/,
                maxlength:12,
            },
            address: "required",
            area: "required",
            city: "required"

        },
        message: {
            fname: "This field is required",
            lname: "This field is required",
            email:{
                required:"This field is required",
                email:"This Email is not Valid"
            },
            mnum:{
                required:"This field is required",
                number_pattern:"Only Digits Numers Allow",
                maxlength:"Maximum Length of Numbers is 12"
            },
            address:"This field is required",
            area:"This field is required",
            city:"This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });


    // $.extend($.validator.messages, {
    //     extension: "'Sorry! You have selected an invalid format file.",
    // });
    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    },"'Sorry! You cannot upload the file max than 5MB' File");

    $("#blog").validate({
        focusInvalid: false,
        rules: {
            title: {
                required:true,
                maxlength: 255
            },
            description: {
                required: true
            },
            post_image: {
                extension:"jpg,jpeg,png,gif,bmp,tif,svg",
                filesize: 2048
            }
        },
        message: {
            name: {
                required:"This field is required",
                maxlength: "Maximum limit is 255 Words"
            },
            email:{
                required:"This field is required"
            },
            post_image:{
                extension:"Only image format is allowed"
            }
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });
    $("#csv_import").validate({
        focusInvalid: false,
        rules: {
            imported_file: {
                required: true,
                extension:"csv,xlsx"
            }
        },
        message: {
            imported_file:{
                required: "This field is required",
                extension:"Only .csv and .xlsx file are allowed"
            }
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $("#inquiry_form").validate({
        focusInvalid: false,
        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email:true
            },
            telephone: {
                required: true,
                number_pattern: /^[0-9 -]+$/,
            },
            message:"required"
        },
        message: {
            fname: "This field is required",
            lname: "This field is required",
            email:{
                required:"This field is required"
            },
            telephone:{
                required: "This field is required"
            },
            message: "This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $("#registerform").validate({
        focusInvalid: false,
        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email:true
            },
            password:{
                required:true,
                minlength: 6
            },
            password_confirmation:{
                required:true,
                equalTo:"#password"
            },
            mobile: {
                required: true,
                number_pattern: /^[0-9-]+$/,
            },
            address:"required",
            country:"required",
            cnic:{
                required:true,
                number_pattern: /^[0-9-]+$/,
            }
        },
        message: {
            fname: "This field is required",
            lname: "This field is required",
            email:{
                required:"This field is required"
            },
            password:"This field is required",
            password_confirmation:"This field is required",
            mobile:{
                required: "This field is required"
            },
            address: "This field is required",
            country:"This field is required",
            cnic:"This field is required",
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $("#tax-calculator").validate({
        focusInvalid: false,
        rules: {
            salary: "required"
        },
        message: {
            salary: "This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $("#send_email").validate({
        focusInvalid: false,
        rules: {
            message: "required"
        },
        message: {
            message: "This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $("#send_sms").validate({
        focusInvalid: false,
        rules: {
            message: {
                required: true,
                max: 60
            }
        },
        message: {
            message: "This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $("#mobile_confirm").validate({
        focusInvalid: false,
        rules: {
            mobile: {
                required: true
            },
            confirm_mobile:{
                required: true
            }
        },
        message: {
            mobile: "This field is required",
            confirm_mobile: "This field is required"
        },
        invalidHandler: function (form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        success: function (element) {

        }
    });

    $(document).on('click','#new_tag',function(){
        $(this).attr('id','exis_tag');
        $(this).text('Use Existing Tags');
        $('#new_tag_div').show();
        $('#exis_tag_div').hide();
        $('#exis_tags').val('');
        $('#exis_tags').attr('id','exis_tags_hide');
        $('#new_tags_hide').attr('id','exis_tags');
    });

    $(document).on('click','#exis_tag',function () {
        $(this).attr('id','new_tag');
        $(this).text('Add New Tag');
        $('#new_tag_div').hide();
        $('#exis_tag_div').show();
        $('#exis_tags').val('');
        $('#exis_tags').attr('id','new_tags_hide');
        $('#exis_tags_hide').attr('id','exis_tags');
    });

    $('#add_tag').on('click',function () {

        var selected_tag = $('#exis_tags').val();
        if (selected_tag != "" && selected_tag != undefined){
            $('#select_tag').append('<div class="alert alert-info btn-sm final_value" style="width: 20%; float: left; margin-left: 10px;">'+selected_tag+'<button type="button" id="'+selected_tag+'" class="close btn-xs remove_tag" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>');
            $('#input_tag').append('<input type="hidden" name="tags[]" id="'+selected_tag+'" value="'+selected_tag+'">');
        }else{
            return false;
        }
        // console.log(selected_tag);
    });

    $('#add_tag_front').on('click',function () {
        var tag = $('#select_tag').html();
        $('#selected_tag_input').append($('#input_tag').html());
        $('#selected_tags').append(tag);
        $('#select_tag').html('');
        $('#input_tag').html('');
        $('#exis_tags').val('');
        $('#tag').modal('toggle');
        // console.log(tag);
    });

    $(document).on('click','#selected_tags .remove_tag',function () {
        $('#'+$(this).attr('id')).remove();
        $(this).parent().remove();
    });

    $(document).on('click','.email_send',function () {
        var email = $(this).data('email');
        var user_id = $(this).data('id');
        var validate = $('#send_email').validate();
        validate.resetForm();
        $('#email_modal input[name=email]').val('');
        $('#email_modal input[name=email]').val(email);
        $('#email_modal input[name=user_id]').val('');
        $('#email_modal input[name=user_id]').val(user_id);
        $('#email_modal').modal('toggle');
    });

    $(document).on('click','.sms_send',function () {
        var sms = $(this).data('mobile');
        var user_id = $(this).data('id');
        var validate = $('#send_sms').validate();
        validate.resetForm();
        $('#sms_modal input[name=mobile]').val('');
        $('#sms_modal #message').val('');
        $('#sms_modal input[name=mobile]').val(sms);
        $('#sms_modal input[name=user_id]').val('');
        $('#sms_modal input[name=user_id]').val(user_id);
        $('#sms_modal').modal('toggle');
    });

    $(document).on('click','#select_tag .remove_tag',function () {
        $('#input_tag #'+$(this).attr('id')).remove();
        $(this).parent().remove();
    });

    $(document).on('click','#save_preview',function (e) {
        e.preventDefault();
        if ($('#blog').validate()) {
            $('form#blog').append('<input type="hidden" name="preview" value="save & preview">');
            $('#blog_submit').trigger('click');
        }
    });

    $(document).ready(function(){
        $('.mobile').mask("9999-9999999");
        $('.cnic').mask('99999-9999999-9');

        $('.pagination').addClass('custom-pagination list-inline text-center text-uppercase mt-4 animation-duration: 0.6s; animation-delay: 0.1s;')
        $('#social-links ul').addClass('social-share list-inline mb-0 text-lg-right')
        $('#social-links li:nth-child(1) a').addClass(' linkedin')
        $('#social-links li:nth-child(2) a').addClass(' google')
        $('#social-links li:nth-child(3) a').addClass(' twitter')
        $('#social-links li:nth-child(4) a').addClass(' facebook')
        $('#social-links li:nth-child(4) a').empty();
        $('#social-links li:nth-child(3) a').empty();
        $('#social-links li:nth-child(2) a').empty();
        $('#social-links li:nth-child(1) a').empty();
        $('#social-links li:nth-child(4) a').append('<i class="fab fa-facebook"></i>');
        $('#social-links li:nth-child(3) a').append('<i class="fab fa-twitter"></i>');
        $('#social-links li:nth-child(2) a').append('<i class="fab fa-google-plus-g"></i>');
        $('#social-links li:nth-child(1) a').append('<i class="fab fa-linkedin-in"></i>');


    });

    $(document).on('click','#agent_submit',function(){
        if($('#agent').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#blog_submit',function(){
        if($('#blog').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#customer_submit',function(){
        if($('#customer').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#inquiry_reply_submit',function(){
        if($('#inquiry_reply').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#csv_import_submit',function(){
        if($('#csv_import').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#loginform_submit',function(){
        if($('#loginform').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#registerform_submit',function(){
        if($('#registerform').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#send_email_submit',function(){
        if($('#send_email').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#send_sms_submit',function(){
        if($('#send_sms').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#inquiry_form_submit',function(){
        if($('#inquiry_form').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#contact_form_submit',function(){
        if($('#contact_form').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','#mobile_confirm_submit',function(){
        if($('#mobile_confirm').valid()){
            $(".se-pre-con").show();
        }
    });
    $(document).on('click','.pg_load',function(){
        $(".se-pre-con").show();
    });
    $(document).on('click','#change_mobile',function(){
        $('#mobile').val('');
        $('#mobile_field').show('');
        $('#mobile_verify_field').hide('');

    });
    $(document).on('click','#schedule_btn',function () {
        $('#schedule .error').text("");
        $('#schedule').modal();
    });
    $(document).on('click','#schedule_blog',function () {
        var today_date = new Date();
        var month = 1+today_date.getMonth();
        var a = today_date.getFullYear()+'/'+("0" + month).slice(-2)+'/'+today_date.getDate();
        if (Date.parse(a) > Date.parse($('#start_date').val())) {
            $('#schedule .error').text("");
            $('#schedule .error').text("Start date should be greater or equal to today's date");
        }else{
            if (Date.parse($('#start_date').val()) > Date.parse($('#end_date').val())) {
                $('#schedule .error').text("");
                $('#schedule .error').text("End date must be greater than start date");
            }else{
                $('input[name=publish_start_date]').val($('#start_date').val());
                $('input[name=publish_end_date]').val($('#end_date').val());
                $('#schedule').modal('toggle');
            }
        }
    });

    CKEDITOR.replace('description');




});

