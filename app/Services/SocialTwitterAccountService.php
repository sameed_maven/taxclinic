<?php

namespace App\Services;
use App\Customer;
use App\Role;
use App\SocialAccount;
use App\SocialTwitterAccount;
use App\User;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialTwitterAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('twitter')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            $user = User::find($account->user_id);
            return $user;
        } else {

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $password = rand(1,10000);
                $user = new User();
                $user->email = $providerUser->getEmail();
                $user->name = $providerUser->getName();
                $user->password = bcrypt($password);
                $user->save();

                $user_id = $user->id;
                $user->roles()->attach(Role::where('name','customer')->first());

//                $name = explode(" ", $providerUser->getName());

                $customer = new Customer();
                $customer->user_id = $user->id;
                $customer->first_name = encrypt($providerUser->getName());
                $customer->last_name = '';
                $customer->mobile_number = ' ';
                $customer->phone_number = ' ';
                $customer->address = ' ';
                $customer->country = ' ';
                $customer->cnic = ' ';
                $customer->status = '1';
                $customer->save();
            }

            $account = new SocialAccount();
            $account->user_id = $user->id;
            $account->provider_user_id = $providerUser->getId();
            $account->provider = 'twitter';
            $account->save();

            $content = '<p>Welcome to Tax Clinic</p><br><p>We wanted to create a trustworthy place for you to find everything you need to know about Taxation.</p><br><p>Tax Clinic is providing professional services with ease of communication through online portal with our consultants available. We respond to your queries and submit your tax return professionally. You don’t need to visit any office, upload / share information through our secure portal with respect to your income, tax and wealth etc, our consultants prepare the computation and shared with you before they submit the same to FBR. Our consultants remain vigilant for any notices etc once you remain enrolled with us. Moreover, relevant updates will be shared with you directly/indirectly affecting you.</p>';
            $data1 = array(
                'subject'=>'Social Registration',
                'email'=>$providerUser->getEmail(),
                'content'=>$content
            );
//        dd($data1);

            Mail::send('email.inquiry_email', $data1, function($message) use($data1){
                $message->to($data1['email']);
                $message->subject($data1['subject']);
            });

            return $user;
        }
    }
}