<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    public function tag(){
        return $this->belongsTo('App\Tag');
    }
}
