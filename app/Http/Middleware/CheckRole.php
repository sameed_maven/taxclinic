<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {

        if($request->user()->hasRole($role) && $role=='admin'){
            return $next($request);
        }
        if($request->user()->hasRole($role) && $role=='agent'){
            return $next($request);
        }
        if($request->user()->hasRole($role) && $role=='consultant'){
            return $next($request);
        }
        if($request->user()->hasRole($role) && $role=='customer'){
            return $next($request);
        }

        //dd($role);

        return back();
    }
}
