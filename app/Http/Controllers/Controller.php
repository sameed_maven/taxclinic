<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

    }

    public function send_sms($phone, $message, $debug=false)
    {
        $user_name = 'taxclinic';
        $password = 'taxclinic99';
        $url = 'http://sms.iisol.pk:80/api/?';

        $urls = 'username='.$user_name;
        $urls .= '&password='.$password;
        $urls .= '&receiver='.urlencode($phone);
        $urls .= '&msgdata='.urlencode($message);

        $urltouse = $url.$urls;
        if ($debug){
            echo "Request: <br>".$urltouse."<br><br>";
        }
        $response = $this->httpRequest($urltouse);

        if ($debug){
//            dd($response);
            echo "Response: <br><pre>".
                str_replace(array("<",">"),array("<",">"),$response).
                "</pre><br>";
        }
        return $response;

    }

    public function httpRequest($url)
    {
        $args;
        $pattern = "/http...([0-9a-zA-Z-.]*).([0-9]*).(.*)/";
        preg_match($pattern,$url,$args);
        $in = "";
//        dd($args);
        $fp = fsockopen($args[1], $args[2], $errno, $errstr, 30);
//        dd($fp);
        if (!$fp)
        {
            return("$errstr ($errno)");
        }
        else
        {
//            dd($fp);
            $out = "GET /$args[3] HTTP/1.1\r\n";
            $out .= "Host: $args[1]:$args[2]\r\n";
            $out .= "User-agent: PHP Web SMS client\r\n";
            $out .= "Accept: */*\r\n";
            $out .= "Connection: Close\r\n\r\n";

            fwrite($fp, $out);
            while (!feof($fp))
            {
                $in.=fgets($fp, 128);
            }
        }
        fclose($fp);
//        $smsapi = explode('<status>',$in);
//        dd($in);
        return($in);
    }
}
