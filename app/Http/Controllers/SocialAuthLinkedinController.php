<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Role;
use App\SocialAccount;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use Socialite;
use Auth;
use Exception;



class SocialAuthLinkedinController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('linkedin')->redirect();
    }


    public function callback()
    {
//        dd(Socialite::driver('linkedin')->user());
        try {
            $linkdinUser = Socialite::driver('linkedin')->stateless()->user();
            $existUser = User::where('email',$linkdinUser->email)->first();
            $mssage = false;
            if($existUser) {
                Auth::loginUsingId($existUser->id);
                $mssage = "You Have Successfully Login with Linkedin";
            }
            else {
                $password = rand(1,10000);
                $user = new User;
                $user->name = $linkdinUser->name;
                $user->email = $linkdinUser->email;
                $user->password = bcrypt($password);
                $user->save();

                $user->roles()->attach(Role::where('name','customer')->first());

                $name = explode(" ", $linkdinUser->name);

                $customer = new Customer();
                $customer->user_id = $user->id;
                $customer->first_name = encrypt($linkdinUser->name);
                $customer->last_name = ' ';
                $customer->mobile_number = ' ';
                $customer->phone_number = ' ';
                $customer->address = ' ';
                $customer->country = ' ';
                $customer->cnic = ' ';
                $customer->status = '1';
                $customer->save();

                $social_account = new SocialAccount();
                $social_account->user_id = $user->id;
                $social_account->provider_user_id = $linkdinUser->id;
                $social_account->provider = 'linkedin';
                $social_account->save();

                $content = '<p>Welcome to Tax Clinic</p><br><p>We wanted to create a trustworthy place for you to find everything you need to know about Taxation.</p><br><p>Tax Clinic is providing professional services with ease of communication through online portal with our consultants available. We respond to your queries and submit your tax return professionally. You don’t need to visit any office, upload / share information through our secure portal with respect to your income, tax and wealth etc, our consultants prepare the computation and shared with you before they submit the same to FBR. Our consultants remain vigilant for any notices etc once you remain enrolled with us. Moreover, relevant updates will be shared with you directly/indirectly affecting you.</p>';
                $data1 = array(
                    'subject'=>'Social Registration',
                    'email'=>$linkdinUser->email,
                    'content'=>$content
                );

                Mail::send('email.inquiry_email', $data1, function($message) use($data1){
                    $message->to($data1['email']);
                    $message->subject($data1['subject']);
                });

                $mssage = "You Have Successfully Register with Linkedin";

                Auth::loginUsingId($user->id);
            }
            $customer = Customer::where('user_id',auth()->user()->id)->get();
            if ($customer[0]->mobile_number == " "){
                return redirect('/customer/mobile-confirm')->with(array(
                    'no_mobile' => 'mobile number is not listed',
                    'message'=>'You Have Successfully Connected With Linkedin'
                ));
            }elseif ($customer[0]->mobile_confirm_status == 0){
                return redirect('/customer/mobile-confirm')->with(
                    'message','You Have Successfully Connected With Linkedin'
                );
            }else {
                return redirect('/')->with('message', 'You Have Successfully Connected With Linkedin');
            }
        }
        catch (Exception $e) {
            return 'error';
        }
    }
}