<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agent = Agent::with('user')->paginate(10);
        return view('admin.agent',compact('agent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add-agent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password= 123456;
        $user = new User();
        $user->name = $request->fname.'_'.$request->lname;
        $user->email = $request->email;
        $user->password = bcrypt($password);
        $user->save();

        $user->roles()->attach(Role::where('name','agent')->first());

       $agent = new Agent();
        $agent->user_id = $user->id;
        $agent->first_name = $request->fname;
        $agent->last_name = $request->lname;
        $agent->mobile = str_replace('-','',$request->mnum);
        $agent->address = $request->address;
        $agent->area = $request->area;
        $agent->city = $request->city;
        $agent->company = $request->company;
        $agent->status = '1';
        $agent->save();

        return redirect('/admin/agent')->with('message','Agent has been Successfully Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent,$id)
    {
        $agent = Agent::with('user')->find($id);
        return view('admin.edit-agent',compact('agent'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent,$id)
    {
        $agent = Agent::find($id);
        $agent->first_name = $request->fname;
        $agent->last_name = $request->lname;
        $agent->mobile = str_replace('-','',$request->mnum);
        $agent->address = $request->address;
        $agent->area = $request->area;
        $agent->city = $request->city;
        $agent->company = $request->company;
        $agent->save();

        return redirect('/admin/agent')->with('message','Agent Has Been Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        //
    }
}
