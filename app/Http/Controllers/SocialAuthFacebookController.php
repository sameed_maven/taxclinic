<?php

// SocialAuthFacebookController.php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\Services\SocialFacebookAccountService;

class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service)
    {
//        dd(Socialite::driver('facebook')->user());
        $user = $service->createOrGetUser(Socialite::driver('facebook')->stateless()->user());
//        dd($user);
        Auth::login($user);
//        dd('success');
        $customer = Customer::where('user_id',auth()->user()->id)->get();
//        dd($customer);
        if ($customer[0]->mobile_number == " "){
            return redirect('/customer/mobile-confirm')->with(array(
                'no_mobile' => 'mobile number is not listed',
                'message'=>'You Have Successfully Connected With Facebook'
            ));
        }elseif ($customer[0]->mobile_confirm_status == 0){
            return redirect('/customer/mobile-confirm')->with(
                'message','You Have Successfully Connected With Facebook'
            );
        }else {
            return redirect('/')->with('message', 'You Have Successfully Connected With Facebook');
        }
    }
}