<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Session;
use App\User;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request)
    {

        if(auth()->user()->hasRole('admin'))
        {
//            dd(auth()->user());
//            dd(Session::get('user'));
            Session::put('user',auth()->user());
            return redirect('/admin/dashboard');
        }elseif (auth()->user()->hasRole('agent'))
        {
            Session::put('user',auth()->user());
            $user = User::paginate(5);
            return redirect('agent/dashboard',compact('user'));
        }elseif(auth()->user()->hasRole('consultant'))
        {
            Session::put('user',auth()->user());
            return redirect('consultant/dashboard');
        }elseif(auth()->user()->hasRole('customer'))
        {
//            dd($request->user()->id);
            $customer = Customer::where('user_id',$request->user()->id)->get();
//            dd($customer);
            if ($customer[0]->status == 1 && $customer[0]->mobile_confirm_status == 1){
                Session::put('user',auth()->user());
                return redirect('/');
            }elseif ($customer[0]->mobile_confirm_status == 0){
//                dd($customer[0]->mobile_number);
                Session::put('user',auth()->user());
                if ($customer[0]->mobile_number == ""){
                    return redirect('/customer/mobile-confirm')->with('no_mobile','mobile number is not listed');
                }elseif ($customer[0]->mobile_confirm == ""){
                    $code = rand(1000,9999);
                    $message = 'Your mobile verification code is '.$code.'.';
                    $response = $this->send_sms(decrypt($customer[0]->mobile_number),$message);
                    $customer = Customer::where('id',$customer[0]->id)->update([
                        'mobile_confirm'=>$code
                    ]);
                    return redirect('/customer/mobile-confirm');
                }
            }else{
                Auth::logout();
                return redirect('/login')->with('email_confirm','Please confirm your email to access your account');
            }

        }

        return redirect('/agent/dashboard');
    }

}
