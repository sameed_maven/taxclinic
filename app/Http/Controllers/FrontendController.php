<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Customer;
use App\Inquiry;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Mail;
use App\Role;
use Session;
use Sujip\Guid\Guid;

class FrontendController extends Controller
{
    public function contact_mail(Request $request)
    {
        $content = '<p>Name: '.$request->name.'</p><br><p>Email: '.$request->email.'</p><br><p>Mobile: '.$request->telephone.'</p><br><p>Message: '.$request->message.'</p>';
        $data1 = array(
            'subject'=>'Contact Query',
            'email'=>$request->email,
            'content'=>$content
        );

        Mail::send('email.contact_email', $data1, function($message) use($data1){
            $message->to('sameed.ahmed@mavensolutions.net');
            $message->subject($data1['subject']);
        });

        $content = 'We have received your message and would like to thank you for writing to us.  We will reply by email shortly.';

        $data = array(
            'subject'=>'Contact Query',
            'email'=>$request->email,
            'content'=>$content
        );

        Mail::send('email.inquiry_email', $data, function($message) use($data){
            $message->to($data['email']);
            $message->subject($data['subject']);
        });

        return redirect('/contact')->with('contact_message','Your Message Has Been Successfully Send');
    }



    // <Inquiry Email> //

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function  create_customer(Request $request){


        $this->validate($request,[
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'mobile' => 'required',
            'address' => 'required',
            'country' => 'required',
            'cnic' => 'required'

        ]);
        $guid = new Guid();

        $guid = $guid->create();

        $customer = Customer::where('cnic',str_replace('-','',$request->cnic))->get()->toArray();
        if ($customer){
            return redirect()->back()->withInput(Input::all())->with('error','Cnic number is already in use');
        }
        $user = User::create([
            'name' => $request->fname.'_'.$request->lname,
            'email' => $request->email,
            'password' => bcrypt($request->password)

        ]);
        $user->roles()->attach(Role::where('name', 'customer')->first());
        $user_id = $user->id;

        $customer = new Customer();
        $customer->user_id = $user_id;
        $customer->first_name = encrypt($request->fname);
        $customer->last_name = encrypt($request->lname);
        $customer->mobile_number = encrypt(str_replace('-','',$request->mobile));
        $customer->phone_number = encrypt(str_replace('-','',$request->phone));
        $customer->address = encrypt($request->address);
        $customer->country = encrypt($request->country);
        $customer->cnic = str_replace('-','',$request->cnic);
        $customer->email_confirm = $guid;
        $customer->status = '0';
        $customer->save();

        $content = '<p>Thank you for being an integral part of the Tax Clinic  platform.</p><br><p>Please verify your <a href="'.url('/email_confirm')."?".http_build_query(['customer_id'=> base64_encode($guid)]).'" target="_blank"> Email Address</a> so we know that it\'s really you!</p>';

        $data1 = array(
            'subject'=>'Email Confirmation',
            'email'=>$request->email,
            'content'=>$content
        );

        Mail::send('email.inquiry_email', $data1, function($message) use($data1){
            $message->to($data1['email']);
            $message->subject($data1['subject']);
        });

        return redirect('/register')->with('message','Please confirm your email to complete registration process');

    }

    public function email_confirm(Request $request)
    {

        if ($request->customer_id){
            $customer = Customer::where('email_confirm',base64_decode($request->customer_id))->get();

            $customerData = Customer::find($customer[0]->id);
            $customerData->status = '1';
            $customerData->save();

            Auth::loginUsingId($customer[0]->user_id);
            $content = '<p>Thank you for being an integral part of the Tax Clinic  platform. </p><br><p>Tax Clinic is providing professional services with ease of communication through online portal with our consultants available. We respond to your queries and submit your tax return professionally. You don’t need to visit any office, upload / share information through our secure portal with respect to your income, tax and wealth etc, our consultants prepare the computation and shared with you before they submit the same to FBR. Our consultants remain vigilant for any notices etc once you remain enrolled with us. Moreover, relevant updates will be shared with you directly/indirectly affecting you.</p>';
            $data = array(
                'content' => $content,
                'subject' => 'Email Confirmation',
                'email' => Auth::user()->email
            );

            Mail::send('email.inquiry_email', $data, function($message) use($data){
                $message->to($data['email']);
                $message->subject($data['subject']);
            });
            return redirect('/customer/dashboard');


        }else{
            dd('Error');
        }
    }

    public function inquiry_form()
    {
        $inquiry = Inquiry::with('inquiry_reply')->where('status','1')->orderBy('id','desc')->take(3)->get();
//        dd($inquiry[0]->inquiry_reply[0]->message);
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        if (Auth::user()){
            if (Auth::user()->hasRole('customer')){
                $customer = Customer::with('user')->where('user_id',Auth::user()->id)->get();
//                dd($customer);
                return view('frontend.inquiry',compact('customer','footer_blog','inquiry'));
            }else{
                return view('frontend.inquiry',compact('footer_blog','inquiry'));
            }
        }else{
            return view('frontend.inquiry', compact('footer_blog','inquiry'));
        }
    }

    public function inquiry_mail(Request $request)
    {
        if (Auth::user()){
            $customer = Customer::where('user_id',Auth::user()->id)->update([
                'last_name'=>encrypt($request->lname),
                'mobile_number'=>encrypt(str_replace('-','',$request->telephone))
            ]);
            $inquiry = new Inquiry();
            $inquiry->user_id = Auth::user()->id;
            $inquiry->first_name = $request->fname;
            $inquiry->last_name = $request->lname;
            $inquiry->mobile = $request->telephone;
            $inquiry->message = $request->message;
            $inquiry->save();


            $content = '<p>Name: '.$request->fname.' '.$request->lname.'</p><br><p>Email: '.$request->email.'</p><br><p>Mobile: '.$request->telephone.'</p><br><p>Message: '.$request->message.'</p>';
            $data1 = array(
                'subject'=>'Inquiry Query',
                'email'=>$request->email,
                'content'=>$content
            );

            Mail::send('email.inquiry_email', $data1, function($message) use($data1){
                $message->to('abdul.sami@mavensolutions.net');
                $message->subject($data1['subject']);
            });

            $content = 'Thank you for your interest in Tax Clinic. We have received your inquiry, Our Tax Expert will contact you shortly';

            $data = array(
                'subject'=>'Inquiry',
                'email'=>$request->email,
                'content'=>$content
            );

            Mail::send('email.inquiry_email', $data, function($message) use($data){
                $message->to($data['email']);
                $message->subject($data['subject']);
            });

            return redirect('/inquiry')->with('message','Your Message Has Been Successfully Send');
        }else{
            return redirect('/login')->with('email_confirm','Please login/register first to submit inquiry form');
        }
    }

    public function free_tax_cal()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        if (Auth::user()){
            $id = Auth::user()->id;
            return view('frontend.free-income-tax-calculator',compact('id','footer_blog'));
        }else{
            return view('frontend.free-income-tax-calculator',compact('footer_blog'));
        }
    }

    public function free_tax_cal_submit(Request $request)
    {
        $user = User::find($request->user_id);
        $content = '<p>Salary: '.$request->salary.'</p><br><p>Monthly Salary: '.$request->monthly_salary.'</p><br><p>Monthly Tax: '.$request->monthly_tax.'</p><br><p>Salary After Tax: '.$request->salary_after_tax.'</p><br><p>Yearly Salary: '.$request->yearly_salary.'</p><br><p>Yearly Tax: '.$request->yearly_tax.'</p><br><p>Yearly Income After Tax: '.$request->yearly_income_after_tax.'</p>';
        $data = array(
            'content' => $content,
            'subject' => 'Tax Calculator Details',
            'email' => $user->email
        );

        Mail::send('email.free_tax_cal', $data, function($message) use($data){
            $message->to($data['email']);
            $message->subject($data['subject']);
        });

        return redirect('free-tax-cal')->with('message','Your Message Has Been Successfully Send');
    }

    public function home()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.index',compact('footer_blog'));
    }

    public function contact()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.contact',compact('footer_blog'));
    }
    public function income_tax()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.income-tax',compact('footer_blog'));
    }
    public function sales_tax()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.sales-tax',compact('footer_blog'));
    }
    public function provincial_tax()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.provincial-tax',compact('footer_blog'));
    }
    public function corporate()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.corporate',compact('footer_blog'));
    }
    public function business_setup()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.business-setup',compact('footer_blog'));
    }
    public function pp()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.privacy_policy',compact('footer_blog'));
    }
    public function tc()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        return view('frontend.terms_and_conditions',compact('footer_blog'));
    }
    public function smsapi()
    {
        $phone = '+923048124160';
//        $phone[] = '9230481241601';
//        $phone[] = '923462767755';
//        $phone[] = '923048124160';
//        $phone[] = '923048124160';
        $message = 'this is test message';
//        $debug = true;
//        foreach ($phone as $key=>$value) {
//        }
        $response = $this->send_sms($phone, $message);
//        dd($response);
        return $response;
    }

    public function send_sms($phone, $message, $debug=false)
    {
        $user_name = 'taxclinic';
        $password = 'taxclinic99';
        $url = 'http://sms.iisol.pk:80/api/?';

        $urls = 'username='.$user_name;
        $urls .= '&password='.$password;
        $urls .= '&receiver='.urlencode($phone);
        $urls .= '&msgdata='.urlencode($message);

        $urltouse = $url.$urls;
        if ($debug){
            echo "Request: <br>".$urltouse."<br><br>";
        }
        $response = $this->httpRequest($urltouse);

        if ($debug){
//            dd($response);
            echo "Response: <br><pre>".
                str_replace(array("<",">"),array("<",">"),$response).
                "</pre><br>";
        }
        return $response;

    }

    public function httpRequest($url)
    {
        $args;
        $pattern = "/http...([0-9a-zA-Z-.]*).([0-9]*).(.*)/";
        preg_match($pattern,$url,$args);
        $in = "";
//        dd($args);
        $fp = fsockopen($args[1], $args[2], $errno, $errstr, 30);
//        dd($fp);
        if (!$fp)
        {
            return("$errstr ($errno)");
        }
        else
        {
//            dd($fp);
            $out = "GET /$args[3] HTTP/1.1\r\n";
            $out .= "Host: $args[1]:$args[2]\r\n";
            $out .= "User-agent: PHP Web SMS client\r\n";
            $out .= "Accept: */*\r\n";
            $out .= "Connection: Close\r\n\r\n";

            fwrite($fp, $out);
            while (!feof($fp))
            {
                $in.=fgets($fp, 128);
            }
        }
        fclose($fp);
//        $smsapi = explode('<status>',$in);
//        dd($in);
        return($in);
    }
}
