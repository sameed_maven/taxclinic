<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogTag;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::paginate(10);
        return view('admin.blog',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tag = Tag::all();
        return view('admin.add-blog',compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'title' => 'required',
            'description' => 'required',
            'post_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        if ($request->post_image) {
            $image = $request->file('post_image');

            $input['post_image'] = time() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('/images/blog');

            $filename = $image->getClientOriginalName();

            $image->move($destinationPath, $filename);
        }

        if ($request->tags){
            foreach ($request->tags as $tag){
                $tags = Tag::where('name',$tag)->pluck('id')->toArray();
                if ($tags){
                    $tag_id[] = $tags[0];
                }else{
                    $tag_new = new Tag();
                    $tag_new->name = $tag;
                    $tag_new->save();
                    $tag_id[] = $tag_new->id;
                }
            }
        }

        $created = new Blog();
        $created->user_id = Session::get('user')->id;
        $created->title = $request->title;
        $created->content = $request->description;
        if ($request->post_image) {
            $created->image_url = $filename;
        }else{
            $created->image_url = '';
        }
        $created->status = ($request->status==1)?'1':'0';
        if (!empty($request->publish_start_date)){
            $created->publish_start_date = $request->publish_start_date;
            if (!empty($request->publish_end_date)){
                $created->publish_end_date = $request->publish_end_date;
            }
            $created->scheduled_status = '1';
        }
        $created->save();
        if ($request->tags) {
            foreach ($tag_id as $tag_id) {
                $blog_tag = new BlogTag();
                $blog_tag->blog_id = $created->id;
                $blog_tag->tag_id = $tag_id;
                $blog_tag->save();
            }
        }

        if($request->preview){
            return redirect('/'.$request->segment(1).'/blog/preview/'.$created->id);
        }else{
            return redirect('/'.$request->segment(1).'/blog')->with('message','Blog Has been Successfully Created');
        }
    }

    public function publish(Request $request,$id)
    {
        $blog = Blog::find($id);
        $blog->status = '1';
        $save = $blog->save();
        if ($save){
            return redirect('/'.$request->segment(1).'/blog')->with('message','Your Blog Has Been Successfully Published');
        }
    }

    public function unpublish(Request $request,$id)
    {
        $blog = Blog::find($id);
        $blog->status = '0';
        $save = $blog->save();
        if ($save){
            return redirect('/'.$request->segment(1).'/blog')->with('message','Your Blog Has Been Successfully Un-Published');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
       $blogs = Blog::where('status','1')->paginate(10);
       $limited_blog = Blog::limit(5)->where('status','1')->get();

       $footer_blog = Blog::limit(2)->where('status','1')->get();

       return view('frontend.blog', compact('blogs','limited_blog','footer_blog'));

    }

    public function singleblog($id){

        $singleblog =Blog::with('user')->find($id);
        $limited_blog = Blog::limit(5)->where('status','1')->get();
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        $blog_tag = BlogTag::with('tag')->where('blog_id',$id)->get();
        return view('frontend.blog-details', compact('singleblog','blog_tag','limited_blog','footer_blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog, $id)
    {
        $blog = Blog::find($id);
        $blog_tag = BlogTag::with('tag')->where('blog_id',$id)->get();
//        dd($blog_tag[0]->tag->name);
        $tag = Tag::all();
        return view('admin.edit-blog', compact('blog','tag', 'blog_tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog,$id)
    {
        $this->validate($request, [

            'title' => 'required|max:255',
            'description' => 'required',
            'post_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        if ($request->file('post_image')){
            $image = $request->file('post_image');

            $input['post_image'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/images/blog');

            $filename = $image->getClientOriginalName();

            $image->move($destinationPath, $filename);
        }
        if ($request->tags) {
            BlogTag::where('blog_id', $id)->delete();

            foreach ($request->tags as $tag) {
                $tags = Tag::where('name', $tag)->pluck('id')->toArray();
                if ($tags) {
                    $tag_id[] = $tags[0];
                } else {
                    $tag_new = new Tag();
                    $tag_new->name = $tag;
                    $tag_new->save();
                    $tag_id[] = $tag_new->id;
                }
            }
        }


        $created = Blog::find($id);
        $created->title = $request->title;
        $created->content = $request->description;
        if ($request->file('post_image')){
            $created->image_url = $filename;
        }
        $created->status = ($request->status==1)?'1':'0';
        if (!empty($request->publish_start_date)){
            $created->publish_start_date = $request->publish_start_date;
            if (!empty($request->publish_end_date)){
                $created->publish_end_date = $request->publish_end_date;
            }
            $created->scheduled_status = '1';
        }
        $created->save();
        if ($request->tags) {
            foreach ($tag_id as $tag_id) {
                $blog_tag = new BlogTag();
                $blog_tag->blog_id = $created->id;
                $blog_tag->tag_id = $tag_id;
                $blog_tag->save();
            }
        }

        if($request->preview){
            $singleblog = Blog::find($created->id);
            $blog_tag = BlogTag::with('tag')->where('blog_id',$created->id)->get();
            return view('admin.blog_preview',compact('singleblog','blog_tag'));
        }else{
            return redirect('/'.$request->segment(1).'/blog')->with('message','Blog Has been Successfully Updated');
        }
    }

    public function blog_preview($id)
    {
        $singleblog = Blog::find($id);
        $blog_tag = BlogTag::with('tag')->where('blog_id',$id)->get();
        return view('admin.blog_preview',compact('singleblog','blog_tag'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
