<?php

// SocialAuthTwitterController.php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialTwitterAccountService;

class SocialAuthTwitterController extends Controller
{
    /**
     * Create a redirect method to twitter api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Return a callback method from twitter api.
     *
     * @return callback URL from twitter
     */
    public function callback(SocialTwitterAccountService $service)
    {
//        dd(Socialite::driver('twitter')->user());
        $user = $service->createOrGetUser(Socialite::driver('twitter')->user());
        auth()->login($user);
//        dd('there');
        $customer = Customer::where('user_id',auth()->user()->id)->get();
        if ($customer[0]->mobile_number == " "){
            return redirect('/customer/mobile-confirm')->with(array(
                'no_mobile' => 'mobile number is not listed',
                'message'=>'You Have Successfully Connected With Twitter'
            ));
        }elseif ($customer[0]->mobile_confirm_status == 0){
            return redirect('/customer/mobile-confirm')->with(
                'message','You Have Successfully Connected With Twitter'
            );
        }else {
            return redirect('/')->with('message', 'You Have Successfully Connected With Twitter');
        }
    }
}