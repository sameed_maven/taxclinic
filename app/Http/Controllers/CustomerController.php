<?php

namespace App\Http\Controllers;

use App\AgentCustomer;
use App\Blog;
use App\Customer;
use App\Mail\SendEmailMailable;
use App\MessageDetail;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use function Psy\debug;

class CustomerController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $customer= array();
        $customerAgent = AgentCustomer::where('agent_id',$id)->get()->toArray();
        if ($customerAgent) {
            foreach ($customerAgent as $agent) {
                $customer[] = Customer::with('user')->where('id', $agent['customer_id'])->get()->toArray();
            }
//            dd($customer);
//        $customer = Customer::paginate(10);
            return view('admin.customer',compact('customer'));
        }else{
            return view('admin.customer',compact('customer'));
        }

    }

    public function import(Request $request, $id)
    {
        $path = $request->file('imported_file')->getRealPath();
        $datas = array_map('str_getcsv', file($path));
        if ($datas[0][0]=='first_name' && $datas[0][1]=='last_name' && $datas[0][2]=='mobile_number' && $datas[0][3]=='phone_number' && $datas[0][4]=='address' && $datas[0][5]=='country' && $datas[0][6]=='cnic' && $datas[0][7]=='email'){
            $csv = array_slice($datas,1);

            $message = array();
            $message_data = '';
            foreach ($csv as $key => $data){
                $email = User::where('email',$data[7])->get()->toArray();
                $cnic = Customer::where('cnic',$data[6])->get()->toArray();

                if ($email){
                    $message['error'][] = $data[7];
                }elseif($cnic){
//                    dd($data[0]);
                    $message_data .= '<p class="alert alert-danger">'.$data[6].'  This CNIC number is already in use.</p>';
                }else{
                    $password = 123456;
                    $user = new User();
                    $user->name = $data[0].'_'.$data[1];
                    $user->email = $data[7];
                    $user->password = bcrypt($password);
                    $user->save();

                    $user->roles()->attach(Role::where('name', 'customer')->first());

                    $customer = new Customer();
                    $customer->user_id = $user->id;
                    $customer->first_name = ($data[0]!='')?encrypt($data[0]):' ';
                    $customer->last_name = ($data[1]!='')?encrypt($data[1]):' ';
                    $customer->mobile_number = ($data[2]!='')?encrypt($data[2]):' ';
                    $customer->phone_number = ($data[3]!='')?encrypt($data[3]):' ';
                    $customer->address = ($data[4]!='')?encrypt($data[4]):' ';
                    $customer->country = ($data[5]!='')?encrypt($data[5]):' ';
                    $customer->cnic = ($data[6]!='')?$data[6]:' ';
                    $customer->status = '1';
                    $customer->save();

                    $agent_customer = new AgentCustomer();
                    $agent_customer->customer_id = $customer->id;
                    $agent_customer->agent_id = $id;
                    $agent_customer->save();


                    Mail::to($data[7])->queue(new SendEmailMailable());
                    $sms = 'You Have Successfully registered.';
                    $response = $this->send_sms($data[2],$sms);

                    if (strpos('Message(s) accepted for delivery',$response) == false){
                        $message_detail = new MessageDetail();
                        $message_detail->user_id = $user->id;
                        $message_detail->mobile = $data[2];
                        $message_detail->message = $sms;
                        $message_detail->response_message = 'delivered';
                        $message_detail->save();

                        $sms_status = Customer::find($customer->id);
                        $sms_status->email_status = '1';
                        $sms_status->sms_status = '1';
                        $sms_status->save();

                    }elseif (strpos('Invalid receiver',$response) == false){
                        $message_detail = new MessageDetail();
                        $message_detail->user_id = $user->id;
                        $message_detail->mobile = $data[2];
                        $message_detail->message = $sms;
                        $message_detail->response_message = 'invalid number';
                        $message_detail->sent_status = '0';
                        $message_detail->save();

                        $sms_status = Customer::find($customer->id);
                        $sms_status->email_status = '1';
                        $sms_status->sms_status = '0';
                        $sms_status->save();
                    }

                    $message['success'][] = $data[7];
                }
            }
            if (isset($message['error'])){
                $message_data .= '<p class="alert alert-danger">'.implode(', ',$message['error']).'  is already registered</p>';
            }elseif(isset($message['success'])){
                $message_data .= '<p class="alert alert-success">'.implode(', ',$message['success']).' is successfully registered</p>';
            }
            return redirect('admin/agent/'.$id.'/customers')->with('message',$message_data);
        }else{
            return back()->with('error','File format is not correct. Please correct format and try again');
        }


    }

    public function send_email(Request $request)
    {
        $content = '<p>'.$request->message.'</p>';
        $data = array(
            'email'=>$request->email,
            'content'=>$content,
        );

        Mail::send('email.inquiry_email', $data, function($message) use($data){
            $message->to($data['email']);
            $message->subject('Email');
        });

        return back()->with('message','<p class="alert alert-success">Email has been successfully send</p>');

    }

    public function sms_send(Request $request)
    {
        $response = $this->send_sms($request->mobile,$request->message);

        if (strpos('Message(s) accepted for delivery',$response) == false){
            $message_detail = new MessageDetail();
            $message_detail->user_id = $request->user_id;
            $message_detail->mobile = $request->mobile;
            $message_detail->message = $request->message;
            $message_detail->response_message = 'delivered';
            $message_detail->save();

            return back()->with('message','<p class="alert alert-success">Message has been sucessfully send</p>');

        }elseif (strpos('Invalid receiver',$response) == false){
            $message_detail = new MessageDetail();
            $message_detail->user_id = $request->user_id;
            $message_detail->mobile = $request->mobile;
            $message_detail->message = $request->message;
            $message_detail->response_message = 'invalid number';
            $message_detail->send_status = '0';
            $message_detail->save();

            return back()->with('error','<p class="alert alert-danger">Invalid Number</p>');
        }
    }

    public function mobile_confirm(Request $request)
    {
        if ($request->confirm_mobile == null){
//            dd('there');
            $code = rand(1000,9999);
            $customer = Customer::where('user_id',auth()->user()->id)->update([
                'mobile_number' => encrypt($request->mobile),
                'mobile_confirm' => $code
            ]);
            $message = 'Your mobile verification code is '.$code.'.';
            $response = $this->send_sms($request->mobile,$message);
            return back();
        }else{
//            dd($request);
            $customer = Customer::where('user_id',auth()->user()->id)
                ->where('mobile_confirm',$request->confirm_mobile)->get();
//        dd($customer);
            if (isset($customer[0]->id)){
//            dd('tere');
                $message = 'Your Mobile Number has been veified';
                $response = $this->send_sms(decrypt($customer[0]->mobile_number),$message);
                $status = Customer::find($customer[0]->id);
                $status->mobile_confirm_status = '1';
                $status->save();

                return redirect('/')->with('message', 'Your Mobile Number Has Been Verified');

            }
//        dd('there12');
            return back()->with('error','Code value is not matched. Please enter again');
        }
    }

    public function confirm_mobile()
    {
        $footer_blog = Blog::limit(2)->where('status','1')->get();
        $mobile_verify = true;
        return view('frontend.mobile_confirm',compact('mobile_verify','footer_blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer,$id,$cus_id)
    {
        $customer = Customer::with('user')->find($cus_id);
//        dd($customer);
        return view('admin.edit-customer',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer,$id,$cus_id)
    {
        $customer = Customer::find($cus_id);
        $customer->first_name = encrypt($request->fname);
        $customer->last_name = encrypt($request->lname);
        $customer->mobile_number = encrypt(str_replace('-','',$request->mnum));
        $customer->phone_number = encrypt(str_replace('-','',$request->pnum));
        $customer->address = encrypt($request->address);
        $customer->country = encrypt($request->country);
        $customer->cnic = str_replace('-','',$request->cnic);
        $customer->save();

        return redirect('admin/agent/'.$id.'/customers')->with('message','<p class="alert alert-success">Customer Has Been Successfully Updated</p>');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }

}
