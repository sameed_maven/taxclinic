<?php

namespace App\Http\Controllers;

use App\Blog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\View\View;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function admin(Request $request)
    {
//        $encrypt = 'sameed';
//        $sam = Crypt::encrypt($encrypt);
//        $decode = base64_decode('3BdBlB5aiWaDWen9adt3zzKZPH6QnDGYBE8JdPu/NTI=');
        $user = User::paginate(2);
//        dd($user->items()[0]['name']);
        if ($request->ajax()) {
            return \Response::json(\View::make('admin_dashboard', array('user' => $user))->render());
        }
        return view('admin.admin_dashboard',compact('user'));
    }

    public function agent()
    {
        return view('agent.agent_dashboard');
    }

    public function customer()
    {
        return view('customer.customer_dashboard');
    }




}
