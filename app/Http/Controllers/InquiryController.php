<?php

namespace App\Http\Controllers;

use App\Inquiry;
use App\InquiryReply;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mail;

class InquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $collection = new Collection;
        $inquiry = Inquiry::with(['user'])->get();
        $quiry = collect($inquiry);
        $inquiry = $quiry->unique('user_id');

//        dd($collection);
        return view('admin.inquiry', compact('inquiry'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $inquiry = new Inquiry();
        $inquiry->user_id = Session::get('user')->id;
        $inquiry->first_name = $req->fname;
        $inquiry->last_name = $req->lname;
        $inquiry->mobile = $req->telephone;
        $inquiry->message = $req->message;
        $inquiry->save();

        return redirect('/inquiry');
    }

    public function inquiry_message($id){
        $data = array();
        $message = Inquiry::where('id',$id)->get(['user_id']);
        $inquiry = Inquiry::with(['user'])->where('user_id',$message[0]->user_id)->get();
        $inquiry_id = Inquiry::where('user_id',$message[0]->user_id)->get(['id'])->last();
//        dd($inquiry_id->id);
        foreach ($inquiry as $inquirys){
            $inquiryReply = InquiryReply::where('inquiry_id',$inquirys->id)->get();
            foreach ($inquiryReply as $reply){
                $data[] = $reply;
            }
        }

        foreach($inquiry as $inquiries){
            $data[] = $inquiries;
        }
//        dd($data);

        $message = collect($data)->sortBy('created_at');
//        dd($message);

        return view('admin.inquiry_message_view',compact('message','inquiry_id'));
    }

    public function save_message(Request $request, $id){
//        dd($request);
        $this->validate($request, [

            'inquiry_message' => 'required',

        ]);
        $msgReply = new InquiryReply();
        $msgReply->inquiry_id = $request->inquiry_id;
        $msgReply->message = $request->inquiry_message;
        $msgReply->save();

        $inquiry = Inquiry::find($request->inquiry_id);
        $inquiry->status = '1';
        $inquiry->save();

        $inquiry = Inquiry::with('user')->find($request->inquiry_id);

        $content = '<p>Thank you for being an integral part of the Tax Clinic  platform. </p><br><p>You ask us the Question:</p><br><p>Question: '.$inquiry->message.'</p><br><p>Answer: '.$request->inquiry_message.'</p>';
        $data1 = array(
            'subject'=>'Inquiry Reply',
            'email'=>$inquiry->user->email,
            'content'=>$content
        );
//        dd($data1);

        Mail::send('email.inquiry_email', $data1, function($message) use($data1){
            $message->to($data1['email']);
            $message->subject($data1['subject']);
        });

        return redirect('admin/inquiry')->with('message','Message Has Been Successfully Send');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Inquiry $inquiry)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(Inquiry $inquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inquiry $inquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inquiry $inquiry)
    {
        //
    }
}
