<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InquiryReply extends Model
{
    public function inquiry(){
        return $this->belongsTo('App\Inquiry');
    }
}
