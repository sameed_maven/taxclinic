<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function inquiry_reply(){
        return $this->HasMany('App\InquiryReply');
    }
}
