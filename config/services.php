<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '964384350401547',
        'client_secret' => 'fe14f1c89b843a19705a3afa40eec0f2',
        'redirect' => 'https://taxclinic.pk/callback_fb',
    ],

    'twitter' => [
        'client_id' => 'XX5g4o7BElscaVObxeRrOKuHv',
        'client_secret' => 'K2KHbV5E0vlrM8h79yJe7yzax1TcsE6q7gLYEvMwJk6m2Fgmvo',
        'redirect' => 'https://taxclinic.pk/callback_tw',
    ],

    'linkedin' => [
        'client_id' => '81tzlzi7o5mbch',
        'client_secret' => '7cdEfV9eZr8J7lTI',
        'redirect' => 'https://taxclinic.pk/callback_ln',
    ],

    'google' => [
        'client_id' => '976503177290-m49rhs06f71sgmhbkj7gutjv0ejvbavq.apps.googleusercontent.com',
        'client_secret' => 'su3a3fTVqB3p3E3Fbe74MePF',
        'redirect' => 'https://taxclinic.pk/callback_gl',
    ],

];
